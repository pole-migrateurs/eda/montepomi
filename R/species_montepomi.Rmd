## Importation des données espèces

```{r setup, include=FALSE}

load_library=function(necessary) 
{
	if(!all(necessary %in% installed.packages()[, 'Package']))
	install.packages(necessary[!necessary %in% installed.packages()[, 'Package']], dep = T)

	for(i in 1:length(necessary))
	library(necessary[i], character.only = TRUE)	
}

load_library("DBI")
load_library("RPostgres")
load_library("stacomirtools")
load_library("tidyverse")
load_library("openxlsx")
load_library("lubridate")
load_library("sf")

server <- "distant"# sinon "localhost" 

if (server == "localhost"){
	options(list(eda.dbname= "eda2.3",
					eda.host= "localhost",
					eda.password= passwordlocal,
					eda.user= userlocal,
					eda.port= 5432))
	
} else {
	if (!exists("mainpass")) stop("mainpass must be provided, configure R at startup")
	options(eda.dbname= "eda2.3",
			eda.host= decrypt_string(hostdistant, mainpass),
			eda.password= decrypt_string(passworddistant, mainpass),
			eda.user= decrypt_string(userdistant, mainpass),
			eda.port= 5432 )
}

con <- dbConnect(Postgres(), 
		dbname="eda2.3", 
		host=getOption("eda.host"),
		port=getOption("eda.port"), 
		user= getOption("eda.user"), 
		password= getOption("eda.password"))
```

On travaille sur les données espèces (saumons, aloses et lamproies). Les données
sont récupérées à partir des fichiers excels de la Loire et la Bretagne.

```{r load_bzh}

#récupération des données Bretagne
library(googledrive)
file <- drive_find(pattern='ajout_donnees_sp.xlsx') 
drive_download(file=file$id, path='./data/ajout_donnees_sp.xlsx',overwrite = TRUE)
data_sp_bzh <- openxlsx::read.xlsx('./data/ajout_donnees_sp.xlsx')
data_sp_bzh <- janitor::clean_names(data_sp_bzh)

save (data_sp_bzh, file = file.path(".","data", "data_sp_bzh.Rdata"))

load(file.path(".","data", "data_sp_bzh.Rdata"))


DBI::Id(
		schema="montepomi",
		table=""
)
```


## Création de la table montepomi.t_station_sta
Cette table comprend son identifiant, le nom de la station, le nom de
cours d'eau et les coordonnées. 
```{r t_station_sta}
DBI::dbExecute(conn=con, "DROP TABLE IF EXISTS montepomi.t_station_sta")
DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_station_sta(
				sta_id SERIAL PRIMARY KEY,
				sta_id_origin TEXT,
				sta_nom TEXT UNIQUE,
				sta_nomce TEXT,
				geom geometry(POINT,4326))")
```


## Création de la table montepomi.t_obs_obs
Cette table comprend son identifiant, l'espèce (sat, ala, et lpm), le bassin verssant, le type de
suivi et les coordonnées.
```{r t_obs_obs}
DBI::dbExecute(conn=con, "DROP TABLE IF EXISTS montepomi.t_obs_obs CASCADE")
DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_obs_obs(
				obs_id INTEGER,
				obs_sta_id INTEGER,
				obs_x NUMERIC,
				obs_y NUMERIC,
				obs_espece TEXT,
				obs_emu TEXT,
				obs_type_suivi TEXT,
				geom geometry(POINT,4326),
				CONSTRAINT c_pk_obs_id_espece_emu PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_fk_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT)")
#Pas de données entrées directement dans la table t_obs_obs (elle va chercher les données entrées dans toutes les tables filles)
```


## Création des tables montepomi.t_obala_ala, montepomi.t_oblpm_lpm et montepomi.t_obsat_sat

Ce sont des tables héritées de la table montepomi.t_obs_obs. Elles comprennent
également l'année et l'effectif pour chaque espèces. Pour l'alose t_obala_ala,
pour la lamproie t_oblpm_lpm et le saumon t_obsat_sat, et pour les bassins
Loire et Bretagne.
```{r}
DBI::dbExecute(conn=con, "DROP TABLE IF EXISTS montepomi.t_obala_ala CASCADE")
DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_obala_ala(
				CONSTRAINT c_pk_ala_obs_id PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_ck_ala_obs_espece CHECK (obs_espece='ala'),
				CONSTRAINT c_uk_ala_x_y unique(obs_espece, obs_x, obs_y),
				CONSTRAINT c_fk_ala_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT)
				INHERITS (montepomi.t_obs_obs)")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obala_ala ALTER COLUMN obs_espece SET DEFAULT 'ala';")

DBI::dbExecute(conn=con, "DROP TABLE IF EXISTS montepomi.t_oblpm_lpm CASCADE")
DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_oblpm_lpm(
				lpm_annee INTEGER,
				lpm_effectif NUMERIC,
				CONSTRAINT c_pk_lpm_obs_id PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_ck_lpm_obs_espece CHECK (obs_espece='lpm'),
				CONSTRAINT c_uk_lpm_x_y_annee unique(obs_espece, obs_x, obs_y, lpm_annee),
				CONSTRAINT c_fk_lpm_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT)
				INHERITS (montepomi.t_obs_obs)")

DBI::dbExecute(conn=con, "DROP TABLE IF EXISTS montepomi.t_obsat_sat CASCADE")
DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_obsat_sat(
				sat_op_id INTEGER,
				sat_date DATE,
				sat_effectif0 INTEGER,
				CONSTRAINT c_pk_sat_obs_id_espece_emu PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_ck_sat_obs_espece CHECK (obs_espece='sat'),
				CONSTRAINT c_uk_sat_date_x_y unique(obs_espece,sat_date,obs_x,obs_y),
				CONSTRAINT c_fk_sat_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT)
				INHERITS (montepomi.t_obs_obs)")
#Pas de données entrées directement dans la table t_obsat_sat (elle va chercher les données entrées dans t_obsatbzh_satb et t_obsatloire_satl)
```

## Création des tables montepomi.t_obalabzh_alab, montepomi.t_oblpmbzh_lpmb et montepomi.t_obsatbzh_satb

Ce sont des tables héritées des tables montepomi.t_obala_ala,
montepomi.t_oblpm_lpm et montepomi.t_obsat_sat avec uniquement les données
concernant la bretagne. Pour le saumon on prendra l'indice de largeur du cours
d'eau
```{r données_bretagne}
DBI::dbExecute(conn=con, "DROP TABLE IF EXISTS montepomi.t_obalabzh_alab CASCADE")
DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_obalabzh_alab(
				CONSTRAINT c_pk_alab_obs_id PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_ck_alab_obs_espece CHECK (obs_espece='ala'),
				CONSTRAINT c_uk_alab_x_y unique(obs_espece, obs_x, obs_y),
				CONSTRAINT c_fk_alab_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT)
				INHERITS (montepomi.t_obala_ala)")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obalabzh_alab ALTER COLUMN obs_espece SET DEFAULT 'ala';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obalabzh_alab ALTER COLUMN obs_emu SET DEFAULT 'FR_Bret';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obalabzh_alab ALTER COLUMN obs_type_suivi SET DEFAULT 'frayere active';")
DBI::dbExecute(conn=con,"CREATE SEQUENCE montepomi.t_obalabzh_alab_obs_id_seq START WITH 1;") 
DBI::dbExecute(conn=con,"ALTER SEQUENCE montepomi.t_obalabzh_alab_obs_id_seq OWNED BY montepomi.t_obalabzh_alab.obs_id;")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obalabzh_alab ALTER COLUMN obs_id SET DEFAULT nextval('montepomi.t_obalabzh_alab_obs_id_seq');")

DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_oblpmbzh_lpmb(
				CONSTRAINT c_pk_lpmb_obs_id PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_ck_lpmb_obs_espece CHECK (obs_espece='lpm'),
				CONSTRAINT c_uk_lpmb_x_y_annee unique(obs_espece, obs_x, obs_y, lpm_annee),
				CONSTRAINT c_fk_lpmb_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT)
				INHERITS (montepomi.t_oblpm_lpm)")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_oblpmbzh_lpmb ALTER COLUMN obs_espece SET DEFAULT 'lpm';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_oblpmbzh_lpmb ALTER COLUMN obs_emu SET DEFAULT 'FR_Bret';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_oblpmbzh_lpmb ALTER COLUMN obs_type_suivi SET DEFAULT 'frayere active';")
DBI::dbExecute(conn=con,"CREATE SEQUENCE montepomi.t_oblpmbzh_lpmb_obs_id_seq START WITH 1;") 
DBI::dbExecute(conn=con,"ALTER SEQUENCE montepomi.t_oblpmbzh_lpmb_obs_id_seq OWNED BY montepomi.t_oblpmbzh_lpmb.obs_id;")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_oblpmbzh_lpmb ALTER COLUMN obs_id SET DEFAULT nextval('montepomi.t_oblpmbzh_lpmb_obs_id_seq');")

DBI::dbExecute(conn=con, "DROP TABLE IF EXISTS montepomi.t_obsatbzh_satb")
DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_obsatbzh_satb(
				satb_largeur NUMERIC,
				CONSTRAINT c_pk_satb_obs_id PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_ck_satb_obs_espece CHECK (obs_espece='sat'),
				CONSTRAINT c_uk_satb_date_x_y unique(obs_espece,sat_date,obs_x,obs_y),
				CONSTRAINT c_fk_satb_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT
				) INHERITS (montepomi.t_obsat_sat)")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatbzh_satb ALTER COLUMN obs_emu SET DEFAULT 'FR_Bret';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatbzh_satb ALTER COLUMN obs_type_suivi SET DEFAULT 'ia';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatbzh_satb ALTER COLUMN obs_espece SET DEFAULT 'sat';")
DBI::dbExecute(conn=con,"CREATE SEQUENCE montepomi.t_obsatbzh_satb_obs_id_seq START WITH 1;") 
DBI::dbExecute(conn=con,"ALTER SEQUENCE montepomi.t_obsatbzh_satb_obs_id_seq OWNED BY montepomi.t_obsatbzh_satb.obs_id;")
#DBI::dbExecute(conn=con,"DROP SEQUENCE t_obsatbzh_satb_obs_id_seq CASCADE;")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatbzh_satb ALTER COLUMN obs_id SET DEFAULT nextval('montepomi.t_obsatbzh_satb_obs_id_seq');")
DBI::dbExecute(conn=con,"CREATE SEQUENCE montepomi.t_obsatbzh_satb_sat_op_id_seq START WITH 1;") 
DBI::dbExecute(conn=con,"ALTER SEQUENCE montepomi.t_obsatbzh_satb_sat_op_id_seq OWNED BY montepomi.t_obsatbzh_satb.sat_op_id;")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatbzh_satb ALTER COLUMN sat_op_id SET DEFAULT nextval('montepomi.t_obsatbzh_satb_sat_op_id_seq');")

```

## Création des tablesmontepomi.t_obalaloire_alal, montepomi.t_oblpmloi_lpml et montepomi.t_obsatloire_satl 

Ce sont des tables héritées des tables montepomi.t_obala_ala,
montepomi.t_oblpm_lpm et montepomi.t_obsat_sat avec uniquement les données
concernant la Loire. Pour le saumon on prendra l'effectif d'âge 1+ en plus de
l'âge 0+
```{r données_loire}
DBI::dbExecute(conn=con, "DROP TABLE IF EXISTS montepomi.t_obalaloire_alal CASCADE")
DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_obalaloire_alal(
				CONSTRAINT c_pk_alal_obs_id PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_ck_alal_obs_espece CHECK (obs_espece='ala'),
				CONSTRAINT c_uk_alal_x_y unique(obs_espece, obs_x, obs_y),
				CONSTRAINT c_fk_alal_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT)
				INHERITS (montepomi.t_obala_ala)")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obalaloire_alal ALTER COLUMN obs_espece SET DEFAULT 'ala';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obalaloire_alal ALTER COLUMN obs_emu SET DEFAULT 'FR_Loir';")
DBI::dbExecute(conn=con,"CREATE SEQUENCE montepomi.t_obalaloire_alal_obs_id_seq START WITH 1;") 
DBI::dbExecute(conn=con,"ALTER SEQUENCE montepomi.t_obalaloire_alal_obs_id_seq OWNED BY montepomi.t_obalaloire_alal.obs_id;")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obalaloire_alal ALTER COLUMN obs_id SET DEFAULT nextval('montepomi.t_obalaloire_alal_obs_id_seq');")

DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_oblpmloi_lpml(
				CONSTRAINT c_pk_lpml_obs_id PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_ck_lpml_obs_espece CHECK (obs_espece='lpm'),
				CONSTRAINT c_uk_lpml_x_y_annee unique(obs_espece, obs_x, obs_y, lpm_annee),
				CONSTRAINT c_fk_lpml_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT)
				INHERITS (montepomi.t_oblpm_lpm)")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_oblpmloi_lpml ALTER COLUMN obs_espece SET DEFAULT 'lpm';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_oblpmloi_lpml ALTER COLUMN obs_emu SET DEFAULT 'FR_Loir';")
DBI::dbExecute(conn=con,"CREATE SEQUENCE montepomi.t_oblpmloi_lpml_obs_id_seq START WITH 1;") 
DBI::dbExecute(conn=con,"ALTER SEQUENCE montepomi.t_oblpmloi_lpml_obs_id_seq OWNED BY montepomi.t_oblpmloi_lpml.obs_id;")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_oblpmloi_lpml ALTER COLUMN obs_id SET DEFAULT nextval('montepomi.t_oblpmloi_lpml_obs_id_seq');")

DBI::dbExecute(conn=con, "DROP TABLE IF EXISTS montepomi.t_obsatloire_satl")
DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_obsatloire_satl(
				satl_effectif1 NUMERIC,
				CONSTRAINT c_pk_satl_obs_id PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_ck_satl_obs_espece CHECK (obs_espece='sat'),
				CONSTRAINT c_uk_satl_date_x_y unique(obs_espece,sat_date,obs_x,obs_y),
				CONSTRAINT c_fk_satl_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT
				) INHERITS (montepomi.t_obsat_sat)")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatloire_satl ALTER COLUMN obs_emu SET DEFAULT 'FR_Loir';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatloire_satl ALTER COLUMN obs_type_suivi SET DEFAULT 'ia';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatloire_satl ALTER COLUMN obs_espece SET DEFAULT 'sat';")
DBI::dbExecute(conn=con,"CREATE SEQUENCE montepomi.t_obsatloire_satl_obs_id_seq START WITH 1;") 
DBI::dbExecute(conn=con,"ALTER SEQUENCE montepomi.t_obsatloire_satl_obs_id_seq OWNED BY montepomi.t_obsatloire_satl.obs_id;")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatloire_satl ALTER COLUMN obs_id SET DEFAULT nextval('montepomi.t_obsatloire_satl_obs_id_seq');")
#DBI::dbExecute(conn=con,"DROP SEQUENCE t_obsat_loire_satl_obs_id_seq CASCADE;")
```

## Création de la table montepomi.t_obsatloiredeversement_ld
c'est une table qui hérite de la table montepomi.t_obs_obs. Elle comprend
également la date et l'effectif des alevins ?
```{r t_obsatloiredeversement_ld}
DBI::dbExecute(conn=con, "DROP TABLE IF EXISTS montepomi.t_obsatloiredeversement_ld")
DBI::dbExecute(conn=con, "CREATE TABLE montepomi.t_obsatloiredeversement_ld(
				ld_op_id INTEGER,
				ld_date DATE,
				ld_effectifdeverse NUMERIC,
				CONSTRAINT c_pk_ld_obs_id PRIMARY KEY (obs_id,obs_espece,obs_emu,obs_type_suivi),
				CONSTRAINT c_ck_ld_obs_espece CHECK (obs_espece='sat'),
				CONSTRAINT c_uk_ld_date_x_y unique(obs_espece,ld_date,obs_x,obs_y),
				CONSTRAINT c_fk_ld_obs_sta_id FOREIGN KEY (obs_sta_id) REFERENCES montepomi.t_station_sta(sta_id) ON UPDATE CASCADE ON DELETE RESTRICT
				) INHERITS (montepomi.t_obs_obs)")
DBI::dbExecute(conn=con, "Comment on COLUMN montepomi.t_obloiredeversement_ld.ld_effectifdeverse IS 'Alevins'")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatloiredeversement_ld ALTER COLUMN obs_type_suivi SET DEFAULT 'deversement';")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatloiredeversement_ld ALTER COLUMN obs_espece SET DEFAULT 'sat';")
DBI::dbExecute(conn=con,"CREATE SEQUENCE montepomi.t_obsatloiredeversement_ld_obs_id_seq START WITH 1;") 
DBI::dbExecute(conn=con,"ALTER SEQUENCE montepomi.t_obsatloiredeversement_ld_obs_id_seq OWNED BY montepomi.t_obsatloiredeversement_ld.obs_id;")
DBI::dbExecute(conn=con,"ALTER TABLE montepomi.t_obsatloiredeversement_ld ALTER COLUMN obs_id SET DEFAULT nextval('montepomi.t_obsatloire_satl_obs_id_seq');")



```

![montepomi species diagram](images/montepomi_species_diagram.png)





```{r setup, include=FALSE}
load_library=function(necessary) {
	
	if(!all(necessary %in% installed.packages()[, 'Package']))
		
		install.packages(necessary[!necessary %in% installed.packages()[, 'Package']], dep = T)
	
	for(i in 1:length(necessary))
		
		library(necessary[i], character.only = TRUE)
	
}

load_library("DBI")

load_library("RPostgres")

load_library("stacomirtools")

load_library("tidyverse")

load_library("openxlsx")

load_library("lubridate")
load_library("sf")

server <- "distant"# sinon "localhost" 

if (server == "localhost"){
	
	options(list(eda.dbname= "eda2.3",
					
					eda.host= "localhost",
					
					eda.password= passwordlocal,
					
					eda.user= userlocal,
					
					eda.port= 5432))
	
} else {
	
	if (!exists("mainpass")) stop("mainpass must be provided, configure R at startup")
	
	options(eda.dbname= "eda2.3",
			
			eda.host= decrypt_string(hostdistant, mainpass),
			
			eda.password= decrypt_string(passworddistant, mainpass),
			
			eda.user= decrypt_string(userdistant, mainpass),
			
			eda.port= 5432 )
	
}

con <- dbConnect(Postgres(), 
		
		dbname="eda2.3", 
		
		host=getOption("eda.host"),
		
		port=getOption("eda.port"), 
		
		user= getOption("eda.user"), 
		
		password= getOption("eda.password"))

```


```{r load_data}
#importation des données (extraire X et Y de geom)
t_obs_obs <- st_read(con,query="select ST_X(geom) x, ST_Y(geom) y,* from montepomi.t_obs_obs")



#carte effectifs 
t_oblpm_lpm<-DBI::dbGetQuery(con,"select * from ontepomi.t_oblpm_lpm") 
t_obsat_sat<-DBI::dbGetQuery(con,"select * from ontepomi.t_obsat_sat") 

print(union_all(data1,data2))

```


#fait des cartes  : de tous les points qu'on a sorti par espèces,
tailles de points variables avec effectifs (pour les saumons et lamproies)




```{r maps}

x <- t_obs_obs$x
y <- t_obs_obs$y


ma_carte <- leaflet::leaflet(data = t_obs_obs) %>%
		leaflet::addTiles()  %>%
		leaflet::addCircleMarkers(
				data = t_obs_obs[t_obs_obs$obs_espece == "sat", ],
				lng = ~x,
				lat = ~y,
				group = "Sat",
				color = "blue",
				radius = 5
		) %>%
		leaflet::addCircleMarkers(
				data = t_obs_obs[t_obs_obs$obs_espece == "lpm", ],
				lng = ~x,
				lat = ~y,
				group = "Lpm",
				color = "red",
				radius = 5
		) %>%
		leaflet::addCircleMarkers(
				data = t_obs_obs[t_obs_obs$obs_espece == "ala", ],
				lng = ~x,
				lat = ~y,
				group = "Ala",
				color = "green",
				radius = 5
		) %>%
		leaflet::addLayersControl(
				baseGroups = c("OpenStreetMap"),
				overlayGroups = c("Sat", "Lpm", "Ala"),  
				options = leaflet::layersControlOptions(collapsed = FALSE)
		)

ma_carte

#TODO faire des autres cartes 
```
