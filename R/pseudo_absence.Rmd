---
title: "pseudo_absence"
output: html_document
date: "2023-06-02"
---


# Setup

On amène le schéma geol de la base geola dans la base eda2.3

```sql
-- CONNECTION EDA2.3



SELECT * FROM france.rn_rna WHERE name LIKE '%Viaur%';
WITH segment_amont AS(
SELECT france.upstream_segments_rn('FR211029') AS idamont)
SELECT * FROM segment_amont
JOIN france.rn_rna ON idamont=idsegment


-- creation d'un foreign data wrapper

CREATE EXTENSION IF NOT EXISTS postgres_fdw;


CREATE SERVER geola_data_wrapper
  FOREIGN DATA WRAPPER postgres_fdw
  OPTIONS (host 'localhost', port '5432', dbname 'geola');
  
-- adapter avec user password pour geola
CREATE USER MAPPING FOR USER
  SERVER geola_data_wrapper
  OPTIONS (user '****', password '********');
  

CREATE SCHEMA geol;
IMPORT FOREIGN SCHEMA geol    
    FROM SERVER geola_data_wrapper
    INTO geol;
    
    
CREATE TABLE montepomi.t_limite_lm AS SELECT * FROM geol.t_limite_lm; --947
CREATE INDEX ON montepomi.t_limite_lm USING gist(geom); 
CREATE INDEX ON montepomi.t_limite_lm USING btree(lm_id);       
```

# Creation of geom_upstream table

**limite** : on récupère les limites depuis la table t_limite_lm

**subset_bre_loi** : on récupère l'ensemble des id segments à partir de rna et
les geom a partir de rn.

**all_within_100** : on projète les limites sur les idsegments les plus proches (dans un rayon de 100m)

**closest_segment** : on selectionne le segment le plus proche du point limite
si il en existe plusieurs dans le rayon de 100m

**upstream_segment** : on repertorie l'ensemble des segments en amont du segment
lié au point limite

**tableau** : On récupère les distances à la mer pour chaques segments.
dist_upstream est la distance entre l'idsegment qui contient la limite et
chaque id segment en amont. On récupère tous les segments dont la distance avec
la limite est supérieure à 10 km en amont (afin d'éviter de générer des
pseudo-absences trop près des points de présence)

On récupère aussi la superficie du bassin versant. 


```sql
DROP TABLE IF EXISTS montepomi.geom_upstream;
CREATE table montepomi.geom_upstream AS (

WITH limite AS (
SELECT lm_id, lm_ta_id, st_transform(geom, 3035) AS geom FROM montepomi.t_limite_lm),

subset_bre_loi AS (
SELECT rn.geom, rna.* FROM france.rn 
JOIN france.rna ON rna.idsegment =rn.idsegment
WHERE (emu = 'FR_Bret' OR emu = 'FR_Loir')
),

-- join with idsegment (rn) 
all_within_100 AS (
SELECT 
lm_id, 
lm_ta_id, 
subset_bre_loi.emu, 
subset_bre_loi.idsegment AS limit_idsegment, 
subset_bre_loi.distanceseam AS distanceseamlimit,  
st_distance(subset_bre_loi.geom, limite.geom) AS dist 
FROM limite JOIN subset_bre_loi ON st_dwithin(subset_bre_loi.geom, limite.geom, 100) 
ORDER BY st_distance(subset_bre_loi.geom, limite.geom)),


closest_segment AS (
SELECT DISTINCT ON (lm_id) * 
FROM all_within_100
),

upstream_segment AS(
SELECT 
france.upstream_segments_rn(limit_idsegment) AS idsegment,
closest_segment.* FROM closest_segment
),

tableau AS(
SELECT 
upstream_segment.*,
rn.geom,
rna.distanceseam,
rna.distanceseam - distanceseamlimit AS dist_upstream,
rna.surfaceunitbvm2
FROM upstream_segment
JOIN france.rn ON upstream_segment.idsegment=rn.idsegment 
JOIN france.rna ON upstream_segment.idsegment=rna.idsegment 
)

SELECT * FROM tableau WHERE dist_upstream > 10000
); --168s
```

# Génération des points de pseudo-absences pour les saumons


**pseudo_absence_sat** : création de la table de pseudo-absences pour les
saumons (lm_ta_id = 2220).

**surfbvamont** : Surface totale du bassin amont à 10 km, calculé et pour chaque
limit_idsegment (point limite)

**totalsurf** : Calcul de la somme des bassins versant amont aux points limites.

**poids_par_limite** : création d'un pourcentage de représentation du bassin
lié à un idsegment par rapport au bassin total pour l'espèce. 

**nobs** : Nombre total d'observation pour les saumons d'après la table
t_obsat_sat

**facteur** : C'est le facteur qui détermine la quatité de pseudo-absence en
plus que les présences qu'on veut générer.

**nupstream** : Nombre d'idsegments total sur lesquels on veut répartir les
pseudo_absences.

**calcul** : Nombre de pseudo-absences (n_pseudoabsence) qu'on veut générer.

**tableau_de_tirage** : Génère tous les points de pseudo-absences au prorata de
la taille du bassin versant. 


 ```sql
 DROP TABLE IF EXISTS montepomi.pseudo_absence_sat;
CREATE TABLE montepomi.pseudo_absence_sat AS(
WITH
surfbvamont AS (
SELECT limit_idsegment, 
sum(surfaceunitbvm2) AS surfbvm2_10km  
FROM 
montepomi.geom_upstream GROUP BY limit_idsegment
WHERE lm_ta_id = 2220
), 
-- 1 ligne
totalsurf AS (
SELECT sum(surfaceunitbvm2) surfbvm2_10kmtot FROM montepomi.geom_upstream
WHERE lm_ta_id = 2220
),

poids_par_limite AS(
SELECT limit_idsegment, surfbvm2_10km/surfbvm2_10kmtot AS perc 
FROM surfbvamont, totalsurf),

-- nobs pour les saumons
nobs AS (
SELECT count (*) AS npresence FROM montepomi.t_obsat_sat),

facteur AS (SELECT 2 AS facteur),

nupstream AS (SELECT count(*) nupstream FROM montepomi.geom_upstream
WHERE lm_ta_id = 2220),

calcul AS (
SELECT 
  npresence,
  facteur,
  npresence*facteur AS n_pseudoabsence,
  nupstream
  FROM 
  nobs, 
  facteur, 
  nupstream),
--SELECT * FROM calcul  

tableau_de_tirage AS (
SELECT geom_upstream.*,
round(perc*n_pseudoabsence) AS nobs_a_tirer,
ROW_NUMBER() OVER (PARTITION BY lm_id ORDER BY random()) AS points
FROM calcul, 
poids_par_limite 
JOIN montepomi.geom_upstream ON geom_upstream.limit_idsegment=poids_par_limite.limit_idsegment
WHERE lm_ta_id = 2220)

SELECT idsegment,
      lm_id,
      lm_ta_id,
      emu,
      limit_idsegment,
      distanceseamlimit,
      round(dist_upstream) AS dist_upstreamm,
      nobs_a_tirer,         
      ST_LineInterpolatePoint(st_linemerge(geom), 0.5) AS geom
FROM tableau_de_tirage WHERE points<=nobs_a_tirer );
```


























