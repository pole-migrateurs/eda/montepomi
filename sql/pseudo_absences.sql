-- CONNECTION EDA2.3




WITH segment_amont AS(
SELECT france.upstream_segments_rn('FR211029') AS idamont)
SELECT * FROM segment_amont
JOIN france.rn_rna ON idamont=idsegment


-- creation d'un foreign data wrapper

CREATE EXTENSION IF NOT EXISTS postgres_fdw;


CREATE SERVER geola_data_wrapper
  FOREIGN DATA WRAPPER postgres_fdw
  OPTIONS (host 'localhost', port '5432', dbname 'geola');
  
-- adapter avec user password pour geola
CREATE USER MAPPING FOR USER
  SERVER geola_data_wrapper
  OPTIONS (user '****', password '********');
  

CREATE SCHEMA geol;
IMPORT FOREIGN SCHEMA geol    
    FROM SERVER geola_data_wrapper
    INTO geol;
    
-- JOIN WITH limit lm_id 622
-- test is 2154

-- TODO dans R select randomly.
--  TODO recuperer point central idsegment pour creer geom point des pseudo absences

   
-- création table segments_distance (dans public)
  
-- copie de la table de l'autre serveur
DROP TABLE IF EXISTS montepomi.t_limite_lm;
CREATE TABLE montepomi.t_limite_lm AS SELECT * FROM geol.t_limite_lm; --940--1005 22/11
CREATE INDEX ON montepomi.t_limite_lm USING gist(geom); 
CREATE INDEX ON montepomi.t_limite_lm USING btree(lm_id);   

-- table dees pseudo absences saumon



DROP TABLE IF EXISTS montepomi.geom_upstream;
CREATE table montepomi.geom_upstream AS (

WITH limite AS (
SELECT lm_id, lm_presence, lm_ta_id, st_transform(geom, 3035) AS geom 
FROM montepomi.t_limite_lm WHERE lm_presence = FALSE),

subset_bre_loi AS (
SELECT rn.geom, rna.* FROM france.rn 
JOIN france.rna ON rna.idsegment =rn.idsegment
WHERE (emu = 'FR_Bret' OR emu = 'FR_Loir')
),

-- join with idsegment (rn) 
all_within_100 AS (
SELECT 
lm_id, 
lm_ta_id, 
lm_presence,
subset_bre_loi.emu, 
subset_bre_loi.idsegment AS limit_idsegment, 
subset_bre_loi.distanceseam AS distanceseamlimit,  
st_distance(subset_bre_loi.geom, limite.geom) AS dist 
FROM limite JOIN subset_bre_loi ON st_dwithin(subset_bre_loi.geom, limite.geom, 100) 
ORDER BY lm_ta_id, lm_id, st_distance(subset_bre_loi.geom, limite.geom)),


closest_segment AS (
SELECT DISTINCT ON (lm_ta_id, lm_id) * 
FROM all_within_100
),

upstream_segment AS(
SELECT 
france.upstream_segments_rn(limit_idsegment) AS idsegment,
closest_segment.* FROM closest_segment
),

tableau AS(
SELECT 
upstream_segment.*,
rn.geom,
rna.distanceseam,
rna.distanceseam - distanceseamlimit AS dist_upstream,
rna.surfaceunitbvm2
FROM upstream_segment
JOIN france.rn ON upstream_segment.idsegment=rn.idsegment 
JOIN france.rna ON upstream_segment.idsegment=rna.idsegment 
)

--Récupérer tous les segments dont la distance avec la limite est supérieure à 10 km en amont (éviter générer des pseudo-absences trop près des points de présence) )

SELECT * FROM tableau WHERE dist_upstream > 10000
); --4m21  68273 --70299 (22/06/2023) --51047 (22/11/2023)



-- Tirage aléatoire des donnéees d'absence à partir des bassins amont 
-- on calcule le nombre de presence
-- on multiplie par facteur (ici 2)
-- On obtient un nb de lignes à répartir
-- on calcule par limite, la taille du bassin amont, le pourcentage de surface de ce bassin amont par 
-- rapport à la surface totale. 
-- On génère des numeros de lignes de manière aléatoire
-- le nombre de lignes à répartir * le pourcentage de surface de chaque bassin donne le nombre
-- de lignes à tirer dans chaque sous-bassin. 


--SAUMON
-- surface de chaque bassin à 10 km
DROP TABLE IF EXISTS montepomi.pseudo_absence_sat;
CREATE TABLE montepomi.pseudo_absence_sat AS(
WITH
surfbvamont AS (
SELECT limit_idsegment, 
sum(surfaceunitbvm2) AS surfbvm2_10km  
FROM 
montepomi.geom_upstream 
WHERE lm_ta_id = 2220
GROUP BY limit_idsegment
), 
-- 1 ligne
totalsurf AS (
SELECT sum(surfaceunitbvm2) surfbvm2_10kmtot FROM montepomi.geom_upstream
WHERE lm_ta_id = 2220
),

poids_par_limite AS(
SELECT limit_idsegment, surfbvm2_10km/surfbvm2_10kmtot AS perc 
FROM surfbvamont, totalsurf),

-- nobs pour les saumons

transformation as (
select date_part('year',sat_date) as year,*  from montepomi.t_obsat_sat ),

nobs AS (
SELECT year, count (*) AS npresence FROM transformation
where year is not null
group by year),

facteur AS (SELECT 2 AS facteur),

nupstream AS (SELECT count(*) nupstream FROM montepomi.geom_upstream
WHERE lm_ta_id = 2220),

calcul AS (
SELECT 
  npresence,
  facteur,
  npresence*facteur AS n_pseudoabsence,
  nupstream,
  year
  FROM 
  nobs, 
  facteur, 
  nupstream),
--SELECT * FROM calcul  

tableau_de_tirage AS (
SELECT geom_upstream.*, year,
round(perc*n_pseudoabsence) AS nobs_a_tirer,
ROW_NUMBER() OVER (PARTITION BY lm_id, year ORDER BY random()) AS points
FROM calcul, 
poids_par_limite 
JOIN montepomi.geom_upstream ON geom_upstream.limit_idsegment=poids_par_limite.limit_idsegment
WHERE lm_ta_id = 2220)

SELECT idsegment,
      lm_id,
      lm_presence,
      lm_ta_id,
      emu,
      limit_idsegment,
      distanceseamlimit,
      round(dist_upstream) AS dist_upstreamm,
      nobs_a_tirer,         
      ST_LineInterpolatePoint(st_linemerge(geom), 0.5) AS geom,
      ST_X(ST_TRANSFORM(ST_LineInterpolatePoint(st_linemerge(geom), 0.5),4326)),
      ST_Y(ST_TRANSFORM(ST_LineInterpolatePoint(st_linemerge(geom), 0.5),4326)),
      year
FROM tableau_de_tirage WHERE points<=nobs_a_tirer AND lm_presence = FALSE ); --2973-- 15140 END montepomi.pseudo_absence_sat 
-- 13639 (22/06/2023) 13087 15097 (22/11/2023)



--ALOSE 
-- surface de chaque bassin à 10 km
DROP TABLE IF EXISTS montepomi.pseudo_absence_ala;
CREATE TABLE montepomi.pseudo_absence_ala AS(
WITH
surfbvamont AS (
SELECT limit_idsegment, 
sum(surfaceunitbvm2) AS surfbvm2_10km  
FROM 
montepomi.geom_upstream 
WHERE lm_ta_id IN (2055 ,2056,2057)
GROUP BY limit_idsegment
), 
-- 1 ligne
totalsurf AS (
SELECT sum(surfaceunitbvm2) surfbvm2_10kmtot FROM montepomi.geom_upstream
WHERE lm_ta_id = 2055 OR lm_ta_id = 2056 OR lm_ta_id = 2057
),

poids_par_limite AS(

SELECT limit_idsegment, surfbvm2_10km/surfbvm2_10kmtot AS perc  FROM surfbvamont, totalsurf),

-- nobs pour les aloses
nobs AS (
SELECT ala_annee as year, count (*) AS npresence FROM montepomi.t_obala_ala
where ala_annee is not null
group by ala_annee),

facteur AS (SELECT 4 AS facteur),

nupstream AS (SELECT count(*) nupstream FROM montepomi.geom_upstream
WHERE lm_ta_id = 2055 OR lm_ta_id = 2056 OR lm_ta_id = 2057),

calcul AS (
SELECT 
  npresence,
  facteur,
  npresence*facteur AS n_pseudoabsence,
  nupstream,
  year
  FROM 
  nobs, 
  facteur, 
  nupstream),
--SELECT * FROM calcul  

tableau_de_tirage AS (
SELECT geom_upstream.*,year,
round(perc*n_pseudoabsence) AS nobs_a_tirer,
ROW_NUMBER() OVER (PARTITION BY lm_id, year ORDER BY random()) AS points
FROM calcul, 
poids_par_limite 
JOIN montepomi.geom_upstream ON geom_upstream.limit_idsegment=poids_par_limite.limit_idsegment
WHERE lm_ta_id = 2055 OR lm_ta_id = 2056 OR lm_ta_id = 2057)

SELECT idsegment,
      lm_id,
      lm_ta_id,
      emu,
      limit_idsegment,
      distanceseamlimit,
      round(dist_upstream) AS dist_upstreamm,
      nobs_a_tirer,         
      ST_LineInterpolatePoint(st_linemerge(geom), 0.5) AS geom,
      ST_X(ST_TRANSFORM(ST_LineInterpolatePoint(st_linemerge(geom), 0.5),4326)),
      ST_Y(ST_TRANSFORM(ST_LineInterpolatePoint(st_linemerge(geom), 0.5),4326)),
      year
FROM tableau_de_tirage WHERE points<=nobs_a_tirer ); --1524-- END montepomi.pseudo_absence_ala --548

--on a 526 alose dans les observation (c'est pas 2 fois plus), c'est surement que des grandes aloses 
--394 --388 (22/06/2023) --290

--LAMPROIE
-- surface de chaque bassin à 10 km
DROP TABLE IF EXISTS montepomi.pseudo_absence_lpm;
CREATE TABLE montepomi.pseudo_absence_lpm AS(
WITH
surfbvamont AS (
SELECT limit_idsegment, 
sum(surfaceunitbvm2) AS surfbvm2_10km  
FROM 
montepomi.geom_upstream 
WHERE lm_ta_id = 2014
GROUP BY limit_idsegment
), 
-- 1 ligne
totalsurf AS (
SELECT sum(surfaceunitbvm2) surfbvm2_10kmtot FROM montepomi.geom_upstream
WHERE lm_ta_id = 2014
), --75336400000

poids_par_limite AS(
SELECT limit_idsegment, 
    surfbvm2_10km/surfbvm2_10kmtot AS perc  
    FROM surfbvamont, totalsurf),
    
--SELECT sum(perc) FROM poids_par_limite

-- nobs pour les lamproies
nobs AS (
select lpm_annee as year, count (*) AS npresence FROM montepomi.t_oblpm_lpm --4 153
where lpm_annee is not null
group by lpm_annee),

facteur AS (SELECT 2 AS facteur), 

nupstream AS (
SELECT count(*) nupstream FROM montepomi.geom_upstream
WHERE lm_ta_id = 2014), --16 398

calcul AS (
SELECT 
  npresence,
  facteur,
  npresence*facteur AS n_pseudoabsence, --8306
  nupstream,
  year
  FROM 
  nobs, 
  facteur, 
  nupstream),

tableau_de_tirage AS (
SELECT geom_upstream.*,year,
round(perc*n_pseudoabsence) AS nobs_a_tirer,
ROW_NUMBER() OVER (PARTITION BY lm_id, year ORDER BY random()) AS points
FROM calcul, poids_par_limite 
JOIN montepomi.geom_upstream ON geom_upstream.limit_idsegment=poids_par_limite.limit_idsegment
WHERE lm_ta_id = 2014)

SELECT idsegment,
      lm_id,
      lm_ta_id,
      emu,
      limit_idsegment,
      distanceseamlimit,
      round(dist_upstream) AS dist_upstreamm,
      nobs_a_tirer,         
      ST_LineInterpolatePoint(st_linemerge(geom), 0.5) AS geom,
      ST_X(ST_TRANSFORM(ST_LineInterpolatePoint(st_linemerge(geom), 0.5),4326)),
      ST_Y(ST_TRANSFORM(ST_LineInterpolatePoint(st_linemerge(geom), 0.5),4326)),
      year
FROM tableau_de_tirage WHERE points<=nobs_a_tirer 


); --6957 -- END montepomi.pseudo_absence_lpm
--7007 (22/06/2023) --962



/*
WITH limite AS (
SELECT lm_id, lm_ta_id, st_transform(geom, 3035) AS geom FROM montepomi.t_limite_lm),

subset_bre_loi AS (
SELECT rn.geom, rna.* FROM france.rn 
JOIN france.rna ON rna.idsegment =rn.idsegment
WHERE (emu = 'FR_Bret' OR emu = 'FR_Loir')
),



-- join with idsegment (rn) 
all_within_100 AS (
SELECT 
lm_id, 
lm_ta_id, 
subset_bre_loi.emu, 
subset_bre_loi.idsegment AS limit_idsegment, 
subset_bre_loi.distanceseam AS distanceseamlimit,  
st_distance(subset_bre_loi.geom, limite.geom) AS dist 
FROM limite JOIN subset_bre_loi ON st_dwithin(subset_bre_loi.geom, limite.geom, 100) 
WHERE lm_ta_id = 2014
ORDER BY st_distance(subset_bre_loi.geom, limite.geom)),


closest_segment AS (
SELECT DISTINCT ON (lm_id) * 
FROM all_within_100
)

SELECT count (*) OVER (PARTITION BY limit_idsegment)  FROM  closest_segment */



