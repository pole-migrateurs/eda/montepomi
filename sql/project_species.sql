-- requête spaciale pour lier les points de présence

--SAUMON
DROP TABLE IF EXISTS montepomi.observation_sat;
CREATE TABLE montepomi.observation_sat as(

WITH observations as(
SELECT obs_id, 
obs_espece, 
st_transform(geom, 3035) AS geom, 
obs_x,
obs_y, 
obs_type_suivi, 
sat_date,
sat_effectif0 
FROM montepomi.t_obsat_sat),


subset_bre_loi AS (
SELECT rn.geom, rna.* FROM france.rn 
JOIN france.rna ON rna.idsegment =rn.idsegment
WHERE (emu = 'FR_Bret' OR emu = 'FR_Loir')
),


-- join with idsegment (rn) 
all_within_1000 AS (
SELECT 
observations.*,
subset_bre_loi.emu, 
subset_bre_loi.idsegment AS presence_idsegment, 
st_distance(subset_bre_loi.geom, observations.geom) AS dist

FROM observations JOIN subset_bre_loi ON st_dwithin(subset_bre_loi.geom, observations.geom, 1000) 
ORDER BY obs_id, emu, st_distance(subset_bre_loi.geom, observations.geom)) --il manque 7 lignes en bretagne (même en mettant 15000 dans le st_dwithin)


--last_table AS (
SELECT DISTINCT ON (obs_id,emu) * 
FROM all_within_1000) --6288 --6300 --7572 --8475 Attention il manquait emu dans le distinct --END CREATE TABLE

-- voir les points non projetes
--non_projete AS (
--SELECT obs_id FROM montepomi.t_obsat_sat 
--except 
--SELECT obs_id FROM last_table) --43, 18

--SELECT * FROM non_projete
--JOIN montepomi.t_obsat_sat tos ON tos.obs_id = non_projete.obs_id
--) --6263

-- 18 points pts de la table observation avec une geom nulle, on la remplace par la geom de la table station
WITH geom_station as(
SELECT t_obsat_sat.obs_id, 
t_obsat_sat.geom,
t_station_sta.geom AS geom_sta  
FROM montepomi.t_obsat_sat
JOIN montepomi.t_station_sta 
ON t_station_sta.sta_id = t_obsat_sat.obs_sta_id
WHERE t_obsat_sat.geom IS NULL)

UPDATE montepomi.t_obsat_sat SET geom = geom_sta FROM geom_station
WHERE t_obsat_sat.obs_id = geom_station.obs_id --7



--ALOSES
DROP TABLE IF EXISTS montepomi.observation_ala;
CREATE TABLE montepomi.observation_ala as(

WITH observations as(
SELECT obs_id,
obs_espece,
st_transform(geom, 3035) AS geom,
obs_x,
obs_y,
obs_type_suivi,
obs_emu,
ala_annee 
FROM montepomi.t_obala_ala),

subset_bre_loi AS (
SELECT rn.geom, rna.* FROM france.rn 
JOIN france.rna ON rna.idsegment =rn.idsegment
WHERE (emu = 'FR_Bret' OR emu = 'FR_Loir')
),


-- join with idsegment (rn) 
all_within_1000 AS (
SELECT 
observations.*,
subset_bre_loi.emu, 
subset_bre_loi.idsegment AS presence_idsegment, 
st_distance(subset_bre_loi.geom, observations.geom) AS dist 
FROM observations JOIN subset_bre_loi ON st_dwithin(subset_bre_loi.geom, observations.geom, 1000) 
ORDER BY obs_id, emu, st_distance(subset_bre_loi.geom, observations.geom))


--last_table AS (
SELECT DISTINCT ON (obs_id, emu) * 
FROM all_within_1000)  --END CREATE TABLE --506 => 526


/*
 * problemes de projection ALA
 */

UPDATE montepomi.t_obala_ala SET obs_x=st_x(geom), obs_y=st_y(geom) WHERE obs_x>10^4; --0

--LAMPROIES
DROP TABLE IF EXISTS montepomi.observation_lpm;
CREATE TABLE montepomi.observation_lpm as(

WITH observations as(
SELECT obs_id, 
obs_espece, 
lpm_annee,
lpm_effectif,
st_transform(geom, 3035) AS geom,
obs_x,
obs_y,
obs_emu,
obs_type_suivi
FROM montepomi.t_oblpm_lpm),

subset_bre_loi AS (
SELECT rn.geom, rna.* FROM france.rn 
JOIN france.rna ON rna.idsegment =rn.idsegment
WHERE (emu = 'FR_Bret' OR emu = 'FR_Loir')
),


-- join with idsegment (rn) 
all_within_1000 AS (
SELECT 
observations.*,
subset_bre_loi.emu, 
subset_bre_loi.idsegment AS presence_idsegment, 
st_distance(subset_bre_loi.geom, observations.geom) AS dist 
FROM observations JOIN subset_bre_loi ON st_dwithin(subset_bre_loi.geom, observations.geom, 1000) 
ORDER BY obs_id, emu, st_distance(subset_bre_loi.geom, observations.geom))


--last_table AS (
SELECT DISTINCT ON (obs_id, emu) * 
FROM all_within_1000)  --END CREATE TABLE --2968 => 5071


