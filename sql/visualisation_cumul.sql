--Jointure entre dam_rios_p_sp et rn pour récupérer la géométrie des drains RHT et visualiser sous QGIS

drop table montepomi.join_dam_rios_p_sp;
create table montepomi.join_dam_rios_p_sp as (
select d.*, geom
from montepomi.temp_dam_rios_p_sp d
left join france.rn r on r.idsegment=d.idsegment
WHERE d.interval='(2019,2022]'
)