-- récupération des limites des Saumons sur les couches cours d'eau 

-- changement de la clef primaire
ALTER TABLE geola.tj_limite_observation_lo DROP CONSTRAINT c_fk_lo_lm_id;

ALTER TABLE geola.t_limite_lm DROP CONSTRAINT t_limite_pkey;

ALTER TABLE geola.t_limite_lm ADD CONSTRAINT c_pk_t_limite_lm PRIMARY KEY (lm_id,lm_ta_id);
ALTER TABLE geola.tj_limite_observation_lo ADD CONSTRAINT c_fk_lo_lm_id
FOREIGN KEY (lo_lm_id,lo_ta_id) REFERENCES geola.t_limite_lm(lm_id, lm_ta_id);

--insertion des lignes dans geola.t_limite_lm

INSERT
	INTO
	geola.t_limite_lm
(
		lm_id,
		lm_ob_id,
		lm_annee_debut,
		lm_annee_fin,
		lm_presence,
		lm_cours_eau_entier,
		lm_nom_expert,
		lm_date_expert,
		lm_cd_id,
		lm_bv_id,
		lm_tl_id,
		lm_id_roe,
		lm_ic_id,
		lm_ta_id,
		lm_co_id,
		lm_commentaire,
		lm_datelastupdate,
		lm_xl93_2154,
		lm_yl93_2154,
		geom
	)

SELECT
	lm_id,
	NULL AS lm_ob_id,
	lm_annee_debut,
	lm_anne_fin,
	lm_presence,
	cours_eau_entier AS lm_cours_eau_entier,
	lm_nom_expert,
	'2023-04-04' AS lm_date_expert,
	cd_id AS lm_cd_id,
	(SELECT CASE WHEN id_bv='?' THEN NULL
	ELSE id_bv END AS id_bv)::integer AS lm_bv_id,
	--nom_bv,
	lm_tl_id::integer,
	--id_sig,
	lm_roe_id AS lm_id_roe,
	lm_ic_id,
	lm_ta_id,
	NULL AS lm_co_id, -- a maj par une requête spatiale
	commentaires,
	lm_datelastupdate,
	NULL AS lm_xl93_2154,
	NULL AS lm_yl93_2154,
	NULL AS geom
	
FROM
	tempa.t_limitsat JOIN referentiel.tr_cours_d_eau ON cd_code=lm_cd_id; --302


ALTER TABLE geola.t_observation_ob DROP COLUMN ob_coordonnees;

-- insertion des observations dans le referentiel

CREATE TEMPORARY SEQUENCE seq1;
ALTER SEQUENCE seq1 RESTART WITH 238;

WITH obs_ref AS (
SELECT obs1 AS obs FROM tempa.t_limitsat
UNION SELECT obs2 AS obs FROM tempa.t_limitsat
UNION SELECT obs3 AS obs FROM tempa.t_limitsat
UNION SELECT obs4 AS obs FROM tempa.t_limitsat
UNION SELECT obs5 AS obs FROM tempa.t_limitsat)

INSERT INTO referentiel.tr_reference_observation
(ro_id, ro_source)
SELECT nextval ('seq1') AS ro_id,
obs AS ro_source FROM obs_ref WHERE obs NOT IN (SELECT ro_source FROM referentiel.tr_reference_observation);

--DELETE FROM geola.t_observation_ob WHERE ob_ta_id = 2220;

-- insertion des observations saumon à partir de la table t_limitsat
	

CREATE TEMPORARY SEQUENCE seq;
ALTER SEQUENCE seq RESTART WITH 2671;

ALTER TABLE geola.t_observation_ob ADD COLUMN ob_lm_id_temp integer;


WITH obs_ref AS (
SELECT obs1 AS obs,lm_id,ro_id FROM tempa.t_limitsat JOIN referentiel.tr_reference_observation ON obs1=ro_source WHERE obs1 IS NOT NULL 
UNION SELECT obs2 AS obs,lm_id,ro_id FROM tempa.t_limitsat JOIN referentiel.tr_reference_observation ON obs2=ro_source WHERE obs2 IS NOT NULL
UNION SELECT obs3 AS obs,lm_id,ro_id FROM tempa.t_limitsat JOIN referentiel.tr_reference_observation ON obs3=ro_source WHERE obs3 IS NOT NULL
UNION SELECT obs4 AS obs,lm_id,ro_id FROM tempa.t_limitsat JOIN referentiel.tr_reference_observation ON obs4=ro_source WHERE obs4 IS NOT NULL
UNION SELECT obs5 AS obs,lm_id,ro_id FROM tempa.t_limitsat JOIN referentiel.tr_reference_observation ON obs5=ro_source WHERE obs5 IS NOT NULL)

INSERT
	INTO
	geola.t_observation_ob
(
		ob_id,
		ob_id_sig,
		ob_annee_debut,
		ob_annee_fin,
		ob_presence,
		ob_nombre,
		ob_poids,
		ob_comment_obs,
		ob_comment_lieu,
		ob_comment_abond,
		ob_bv_id,
		ob_rh_code,
		ob_sh_code,
		ob_ss_code,
		ob_zh_code,
		ob_cd_code,
		ob_de_code,
		ob_cm_id,
		ob_no_id,
		ob_to_id,
		ob_ro_id,
		ob_ce_id,
		ob_ic_id,
		ob_co_id,
		ob_id_roe,
		ob_codehydro,
		ob_ta_id,
		ob_commentinsert,
		ob_lastupdate,
		geom,
		ob_xl93_2154,
		ob_yl93_2154,
		ob_lm_id_temp
	)

SELECT
	NEXTVAL ('seq') AS ob_id,
	NULL AS ob_id_sig,
	lm_annee_debut,
	lm_anne_fin,
	lm_presence,
	NULL AS ob_nombre,
	NULL AS ob_poids,
	NULL AS ob_comment_obs,
	commentaires AS ob_comment_lieu,
	NULL AS ob_comment_abond,
	--cours_eau_entier,
	--lm_nom_expert,
	--lm_date_expert,
	(SELECT CASE WHEN id_bv='?' THEN NULL
	ELSE id_bv END AS id_bv)::integer AS ob_bv_id,
	NULL AS ob_rh_code,
	NULL AS ob_sh_code,
	NULL AS ob_ss_code,
	NULL AS ob_zh_code,
	NULL AS ob_cd_code,
	NULL AS ob_de_code,
	NULL AS ob_cm_id,
	NULL AS ob_no_id,
	NULL AS ob_to_id,
	obs_ref.ro_id AS ob_ro_id,
	NULL AS ob_ce_id,
	CASE WHEN lm_ic_id IS NULL THEN 4 ELSE lm_ic_id END AS ob_ic_id,
	NULL AS ob_co_id,
	lm_roe_id AS ob_id_roe,
	lm_cd_id AS ob_codehydro,
	lm_ta_id AS ob_ta_id,
	NULL AS ob_commentinsert,
	lm_datelastupdate AS ob_lastupdate,
	NULL AS geom,
	NULL AS ob_xl93_2154,
	NULL AS ob_yl93_2154,
	t_limitsat.lm_id AS ob_lm_id_temp
	
FROM
	tempa.t_limitsat LEFT JOIN obs_ref ON obs_ref.lm_id=t_limitsat.lm_id; --442;
	
--JOIN referentiel.tr_reference_observation ON ro_source=obs_ref.obs;

-- remplacer reference (obs de t_limitsat) par son ob_id

INSERT INTO geola.tj_limite_observation_lo
SELECT ob_lm_id_temp AS lo_lm_id, ob_id AS lo_ob_id, ob_ta_id AS lo_ta_id
FROM t_observation_ob WHERE ob_ta_id = 2220; --442

ALTER TABLE geola.t_observation_ob DROP COLUMN ob_lm_id_temp;


-- jointure entre tr_cours_d_eau, ce_sat et t_limite_lm via cd_code
-- TODO remplir saumon

WITH code_ce AS (
SELECT t_limite_lm.*, tr_cours_d_eau.cd_id, tr_cours_d_eau.cd_code
FROM geol.t_limite_lm
JOIN referentiel.tr_cours_d_eau
ON t_limite_lm.lm_cd_id = tr_cours_d_eau.cd_id
WHERE lm_ta_id = 2220),

join_ce_sat AS (
SELECT DISTINCT ON(code_ce.cd_code) 
code_ce.lm_id::integer AS lm_id,
geol.ce_sat.geom
FROM geol.ce_sat
LEFT JOIN code_ce
ON code_ce.cd_code = ce_sat.cd_code),

jointure_finale AS (SELECT lm_id, St_StartPoint ((st_dump(geom)).geom) geom FROM join_ce_sat)

UPDATE geol.t_limite_lm lm SET geom = jointure_finale.geom FROM jointure_finale 
WHERE (lm.lm_id, lm.lm_ta_id) = (jointure_finale.lm_id,2220)

; --230

-- mise a jour des bassins versants 

WITH bs AS (SELECT * FROM geol.t_limite_lm lm
JOIN referentiel.tr_bassin_versant tbv ON st_intersects(lm.geom, tbv.bv_geom))
UPDATE geol.t_limite_lm SET lm_bv_id = bs.bv_id FROM bs
WHERE t_limite_lm.lm_id = bs.lm_id 
AND t_limite_lm.lm_bv_id is NULL; -- 7


-- some points are not there :


CREATE TABLE tempa.points_nouveaux 
WITH code_ce AS (
SELECT t_limite_lm.*, tr_cours_d_eau.cd_id, tr_cours_d_eau.cd_code
FROM geol.t_limite_lm
JOIN referentiel.tr_cours_d_eau
ON t_limite_lm.lm_cd_id = tr_cours_d_eau.cd_id
WHERE lm_ta_id = 2220),

join_ce_sat AS (
SELECT DISTINCT ON(code_ce.cd_code) 
code_ce.lm_id::integer AS lm_id,
geol.ce_sat.geom
FROM geol.ce_sat
LEFT JOIN code_ce
ON code_ce.cd_code = ce_sat.cd_code),

jointure_finale AS (
SELECT lm_id, St_StartPoint ((st_dump(geom)).geom) geom FROM join_ce_sat)


-- ajout de cours d'eaux pour le modèle

CREATE TABLE tempa.point_nouveaux_sat AS SELECT * FROM tempa.points_nouveaux AS pn 

DELETE FROM tempa.point_nouveaux_sat WHERE lm_id < 1000 AND lm_id !=69; 
UPDATE tempa.point_nouveaux_sat SET lm_id = 1006 WHERE lm_id =69; 
SELECT * FROM tempa.point_nouveaux_sat



WITH additional AS (
SELECT * FROM tempa.point_nouveaux_sat pn 
WHERE lm_id NOT IN (SELECT lm_id FROM geol.t_limite_lm))  -- 1 point : id = 198 sur le bassin gironde
INSERT INTO geol.t_limite_lm
(lm_id,  lm_annee_debut, lm_annee_fin, lm_presence, lm_cours_eau_entier, lm_nom_expert, lm_date_expert, lm_cd_id, lm_bv_id, lm_tl_id, lm_id_roe, lm_ic_id, lm_ta_id, lm_co_id, lm_commentaire, lm_datelastupdate, lm_xl93_2154, lm_yl93_2154, geom)
SELECT lm_id,  2006, 2015, false, false, 'Marion Legrand', '2023-11-22', NULL, NULL, 4, NULL, 4, 2220, 5, 'Limites supplémentaires pour la répartion du Saumon en Loire', '2023-03-31', st_x(geom), st_y(geom), geom
FROM additional; -- 1


WITH ll AS (
SELECT * FROM geol.t_limite_lm WHERE lm_id IN (228,622))
INSERT INTO geol.t_limite_lm
(lm_id,  lm_annee_debut, lm_annee_fin, lm_presence, lm_cours_eau_entier, 
lm_nom_expert, lm_date_expert, lm_cd_id, lm_bv_id, lm_tl_id, lm_id_roe, lm_ic_id, 
lm_ta_id, lm_co_id, lm_commentaire, lm_datelastupdate, lm_xl93_2154, lm_yl93_2154, geom)
SELECT lm_id +1000,  
lm_annee_debut, 
lm_annee_fin, 
FALSE AS lm_presence,
FALSE AS lm_cours_eau_entier, 
'Marion Legrand' AS lm_nom_expert, 
'2023-11-22' AS lm_date_expert, 
lm_cd_id,
lm_bv_id,
lm_tl_id,
lm_id_roe,
lm_ic_id,
2220 AS lm_ta_id, 
lm_co_id, 
'Limites supplémentaires pour la répartion du Saumon en Loire' AS lm_commentaire,
'2023-03-31' AS lm_datelastupdate, 
lm_xl93_2154,
lm_yl93_2154,
geom
FROM ll



WITH ll AS (
SELECT * FROM geol.t_limite_lm WHERE lm_id IN (355,356,1613) AND lm_ta_id=2056)
INSERT INTO geol.t_limite_lm
(lm_id,  lm_annee_debut, lm_annee_fin, lm_presence, lm_cours_eau_entier, 
lm_nom_expert, lm_date_expert, lm_cd_id, lm_bv_id, lm_tl_id, lm_id_roe, lm_ic_id, 
lm_ta_id, lm_co_id, lm_commentaire, lm_datelastupdate, lm_xl93_2154, lm_yl93_2154, geom)
SELECT lm_id +2000,  
lm_annee_debut, 
lm_annee_fin, 
FALSE AS lm_presence,
FALSE AS lm_cours_eau_entier, 
'Marion Legrand' AS lm_nom_expert, 
'2023-11-22' AS lm_date_expert, 
lm_cd_id,
lm_bv_id,
lm_tl_id,
lm_id_roe,
lm_ic_id,
2220 AS lm_ta_id, 
lm_co_id, 
'Limites supplémentaires pour la répartion du Saumon en Loire' AS lm_commentaire,
'2023-03-31' AS lm_datelastupdate, 
lm_xl93_2154,
lm_yl93_2154,
geom
FROM ll; --3 sur la Vilaine

