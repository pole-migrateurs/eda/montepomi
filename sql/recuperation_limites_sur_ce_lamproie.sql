-- récupération des limites sur les couches cours d'eau

SET search_path TO geola, public, referentiel;
SELECT * FROM ce_lpm LIMIT 10;
SELECT count(*) FROM ce_lpm; --198

DROP TABLE IF EXISTS tempa.limite_manquante;
CREATE TABLE tempa.limite_manquante AS SELECT id, St_StartPoint ((st_dump(geom)).geom) geom FROM ce_lpm; --198



SELECT (st_dump(geom)).geom FROM ce_lpm;

-- renvoit les 155 points sans intersection


CREATE TABLE tempa.points_nouveaux AS (
WITH point_amont AS (
SELECT id, St_StartPoint ((st_dump(geom)).geom) geom FROM ce_lpm)

    SELECT * FROM point_amont 
    EXCEPT     
    SELECT point_amont.* FROM point_amont JOIN t_limite_lm lm ON
    st_dwithin(lm.geom, point_amont.geom,500));

   
SELECT count(*)
FROM geola.t_limite_lm
WHERE t_limite_lm.geom IS NOT NULL; --70

UPDATE geola.t_limite_lm SET geom = points_nouveaux.geom FROM tempa.points_nouveaux 
WHERE t_limite_lm.lm_id = points_nouveaux.id AND t_limite_lm.geom IS NULL; --132

SELECT count(*)
FROM geola.t_limite_lm
WHERE t_limite_lm.geom IS NOT NULL;  --202

WITH additional AS (
SELECT * FROM tempa.points_nouveaux pn 
WHERE id NOT IN (SELECT lm_id FROM geola.t_limite_lm))  -- 1 point : id = 198 sur le bassin gironde
INSERT INTO geola.t_limite_lm
(lm_id, lm_ob_id, lm_annee_debut, lm_annee_fin, lm_presence, lm_cours_eau_entier, lm_nom_expert, lm_date_expert, lm_cd_id, lm_bv_id, lm_tl_id, lm_id_roe, lm_ic_id, lm_ta_id, lm_co_id, lm_commentaire, lm_datelastupdate, lm_xl93_2154, lm_yl93_2154, geom)
SELECT id, NULL, 2006, 2015, true, false, 'valeur manquante insérée à partir des cartes', '2023-03-31', 124052, 156, 4, NULL, 1, 2014, 6, 'valeur manquante rajoutée à partir de la carte', '2023-03-31', st_x(geom), st_y(geom), geom
FROM additional; -- 1


-- ajout d'une colonne espèces
ALTER TABLE geola.tj_limite_observation_lo ADD COLUMN lo_ta_id integer;
UPDATE geola.tj_limite_observation_lo SET lo_ta_id=lm_ta_id FROM geola.t_limite_lm WHERE lo_lm_id=lm_id;
ALTER TABLE geola.tj_limite_observation_lo ALTER COLUMN lo_ta_id SET NOT NULL;
