-- récupération des limites des Aloses sur les couches cours d'eau 

ALTER TABLE geola.alose OWNER TO geola;
SET search_path TO geola, public, referentiel;
SELECT * FROM alose LIMIT 10;
SELECT count(*) FROM alose; --64

DROP TABLE IF EXISTS tempa.limite_manquante_ala;
CREATE TABLE tempa.limite_manquante_ala AS SELECT gid, St_StartPoint ((st_dump(geom)).geom) geom FROM alose;

SELECT (st_dump(geom)).geom FROM alose;  

DROP TABLE IF EXISTS tempa.points_nouveaux_ala;
CREATE TABLE tempa.points_nouveaux_ala AS (
WITH point_amont AS (
SELECT lm_id, St_StartPoint ((st_dump(geom)).geom) geom FROM alose)

    SELECT * FROM point_amont 
    EXCEPT     
    SELECT point_amont.* FROM point_amont JOIN t_limite_lm lm ON
    st_dwithin(lm.geom, point_amont.geom,500));
    
SELECT count(*)
FROM geola.t_limite_lm
WHERE t_limite_lm.geom IS NOT NULL; --203 

UPDATE geola.t_limite_lm SET geom = points_nouveaux_ala.geom FROM tempa.points_nouveaux_ala
WHERE t_limite_lm.lm_id = points_nouveaux_ala.lm_id AND t_limite_lm.geom IS NULL; --28

SELECT * FROM tempa.points_nouveaux_ala  
WHERE points_nouveaux_ala.lm_id NOT IN (SELECT lm_id FROM geola.t_limite_lm); -- 0 (tous les ID sont présents dans t_limite_lm)


 