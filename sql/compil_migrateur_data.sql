# script de compilation de la base de donn�es

# TODO copier ici le script de restauration de la base GEOLA


# La g�om�trie est manquante

SELECT * FROM public.observation LIMIT 10;

SELECT st_geometrytype(obb_coordonnees) FROM  public.observation ;

SELECT st_geometrytype(cd_geom) FROM public.tr_cours_d_eau;




UPDATE public.observation SET obb_coordonnees = roe_geom
FROM 
public.tr_roe
WHERE observation.ob_roe_id = tr_roe.roe_id ; --1566


SELECT count(*) FROM public.observation WHERE obb_coordonnees IS NULL; --851

SELECT * FROM public.observation WHERE obb_coordonnees IS NULL; 


-- r�cup�ration de l'identifiant du tron�on � partir du ROE

CREATE INDEX idx_gist_roe_geom ON tr_roe USING gist(roe_geom);
CREATE INDEX idx_gist_cd_geom ON tr_cours_d_eau USING gist(cd_geom);

ALTER TABLE observation ADD COLUMN comment_insert VARCHAR;

UPDATE observation SET comment_insert = NULL;


WITH missing AS (SELECT * FROM observation  
WHERE ob_cd_id IS NULL),

troncons AS (
SELECT * FROM missing 
JOIN tr_roe ON  missing.ob_roe_id = roe_id
JOIN tr_cours_d_eau
 ON st_dwithin(roe_geom, cd_geom, 300))
 
 UPDATE public.observation SET (obb_geomline, ob_cd_id, comment_insert) = (cd_geom, cd_id, 'insertion from roe')
FROM 
troncons
WHERE troncons.ob_id = observation.ob_id ; --71


-- DROP TABLE geola.observation;
ALTER TABLE public.observation SET SCHEMA geola;


-- r�cup�ration du point le plus en amont du cours d'eau.



 

