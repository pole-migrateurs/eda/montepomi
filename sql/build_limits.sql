/*
 * script de re-création de la table geola.t_limites_lm
 * et de la table geol.tj_limite_observation
 * Les données sont traitées au préalable dans R/limite.R
 * 
 */ 



/*
 * TABLE DES LIMITES
 * Quelles sont les références biblio et sources correspondant à la construction des limites.
 */
DROP TABLE IF EXISTS geola.t_limite_lm;
CREATE TABLE geola.t_limite_lm (
  lm_id INTEGER NOT NULL,
  lm_ob_id INTEGER , -- faire le lien via ob_id_sig
  lm_annee_debut INTEGER NOT NULL,
  lm_annee_fin INTEGER NOT NULL,
  lm_presence bool NOT NULL,
  lm_cours_eau_entier bool NOT NULL DEFAULT false,
  lm_nom_expert text NOT NULL,
  lm_date_expert date NOT NULL,
  lm_cd_id INTEGER,
  lm_bv_id INTEGER, -- Cette colonne n'était pas dans la TABLE d'origine elle a été ajoutée en excel
  lm_tl_id INTEGER NOT NULL,
  lm_id_roe TEXT NULL,
  lm_ic_id INTEGER,
  lm_ta_id INTEGER NOT NULL,
  lm_co_id INTEGER,-- Cette colonne n'était pas dans la TABLE d'origine elle a été ajoutée en excel
  lm_commentaire text NULL,
  lm_datelastupdate DATE NOT NULL,
  lm_xl93_2154 NUMERIC,
  lm_yl93_2154 NUMERIC,
  geom geometry("POINT"),
  CONSTRAINT t_limite_pkey PRIMARY KEY (lm_id),
  CONSTRAINT t_limite_lm_ob_id_fkey FOREIGN KEY (lm_ob_id) REFERENCES geola.t_observation_ob(ob_id), 
  CONSTRAINT t_limite_lm_cd_id_fkey FOREIGN KEY (lm_cd_id) REFERENCES referentiel.tr_cours_d_eau(cd_id),
  CONSTRAINT t_limite_lm_bv_id_fkey FOREIGN KEY (lm_bv_id) REFERENCES referentiel.tr_bassin_versant(bv_id),
  CONSTRAINT t_limite_lm_ic_id_fkey FOREIGN KEY (lm_ic_id) REFERENCES referentiel.tr_indice_confiance(ic_id),
  CONSTRAINT t_limite_lm_id_roe_fkey FOREIGN KEY (lm_id_roe) REFERENCES referentiel.tr_roe(roe_id_roe),
  CONSTRAINT t_limite_lm_ta_id_fkey FOREIGN KEY (lm_ta_id) REFERENCES referentiel.tr_taxon(ta_id),
  CONSTRAINT t_limite_lm_co_id_fkey FOREIGN KEY (lm_co_id) REFERENCES referentiel.tr_cogepomi(co_id),
  CONSTRAINT t_limite_lm_tl_id_fkey FOREIGN KEY (lm_tl_id) REFERENCES referentiel.tr_type_limite(tl_id)
);
COMMENT ON COLUMN geola.t_limite_lm.lm_annee_debut IS 'Année de début de l''observation';
COMMENT ON COLUMN geola.t_limite_lm.lm_annee_fin IS 'Année de fin de l''observation';
COMMENT ON COLUMN geola.t_limite_lm.lm_presence IS 'Présence des poissons migrateurs';
COMMENT ON COLUMN geola.t_limite_lm.lm_presence IS 'Est ce que le taxon est présent ?';
COMMENT ON COLUMN geola.t_limite_lm.lm_nom_expert IS 'Nom de / des experts ayant fait l''expertise';
COMMENT ON COLUMN geola.t_limite_lm.lm_date_expert IS 'Date de l''expertise';
COMMENT ON COLUMN geola.t_limite_lm.lm_cd_id IS 'Identifiant du cours d''eau';
COMMENT ON COLUMN geola.t_limite_lm.lm_tl_id IS 'Type de limite';
COMMENT ON COLUMN geola.t_limite_lm.lm_id_roe IS 'Code ROE';
COMMENT ON COLUMN geola.t_limite_lm.lm_ic_id IS 'Indice de confiance de l''observation';
COMMENT ON COLUMN geola.t_limite_lm.lm_ta_id IS 'Code du taxon';
COMMENT ON COLUMN geola.t_limite_lm.lm_co_id IS 'Cogepomi voir referentiel.tr_cogepomi';
COMMENT ON COLUMN geola.t_limite_lm.lm_bv_id IS 'Identifiant du bassin versant';
COMMENT ON COLUMN geola.t_limite_lm.lm_commentaire IS 'Commentaires';
COMMENT ON COLUMN geola.t_limite_lm.lm_datelastupdate IS 'Date de la dernière modification'; -- toujours 01/01/2019 ??
COMMENT ON COLUMN geola.t_limite_lm.geom IS 'geometrie (point) des observations';
ALTER TABLE geola.t_limite_lm OWNER TO geola;

INSERT INTO geola.t_limite_lm(
lm_id,
lm_ob_id,
lm_annee_debut,
lm_annee_fin,
lm_presence,
lm_cours_eau_entier,
lm_nom_expert,
lm_date_expert,
lm_cd_id,
lm_bv_id,
lm_tl_id,
lm_id_roe,
lm_ic_id,
lm_ta_id,
lm_co_id,
lm_commentaire,
lm_datelastupdate,
lm_xl93_2154,
lm_yl93_2154,
geom)
SELECT
lm_id,
ob.ob_id AS lm_ob_id,
lm_annee_debut,
lm_anne_fin,
lm_presence,
cours_eau_entier AS lm_cours_eau_entier,
lm_nom_expert,
lm_date_expert,
cd_id AS lm_cd_id,
id_bv AS lm_bv_id,
lm_tl_id,
lm_roe_id AS lm_id_roe,
lm_ic_id,
ta_id AS lm_ta_id, 
co_id AS lm_co_di,
commentaires AS lm_commentaire,
'2019-01-01' AS lm_datelastupdate,
st_x(ob.geom) AS lm_xl93_2154,
st_y(ob.geom) AS lm_yl93_2154,
ob.geom 
FROM tempa.t_limit_lm 
LEFT JOIN (SELECT DISTINCT ON (ob_id_sig) * FROM geola.t_observation_ob ) ob 
ON ob.ob_id_sig=id_sig::INTEGER
JOIN referentiel.tr_cours_d_eau cd ON cd.cd_code = lm_cd_id; --644


CREATE INDEX ON geola.t_limite_lm USING gist(geom)

SELECT * 
FROM tempa.t_limit_lm 
LEFT JOIN (SELECT DISTINCT ON (ob_id_sig) * FROM geola.t_observation_ob ) ob 
ON ob.ob_id_sig=id_sig::INTEGER
JOIN referentiel.tr_cours_d_eau cd ON cd.cd_code = lm_cd_id;



/*
 * TABLE DE JOINTURE DES OBSERVATIONS ET DES LIMITES
 * Quelles sont les références biblio et sources correspondant à la construction des limites.
 */


ALTER TABLE geola.ce_lpm OWNER TO geola;

DROP TABLE IF EXISTS geola.tj_limite_observation_lo;
CREATE TABLE geola.tj_limite_observation_lo(
lo_lm_id INTEGER NOT NULL,
lo_ob_id INTEGER NOT NULL,
CONSTRAINT c_fk_lo_lm_id FOREIGN KEY (lo_lm_id) REFERENCES geola.t_limite_lm (lm_id),
CONSTRAINT c_fk_lo_ob_id FOREIGN KEY (lo_ob_id) REFERENCES geola.t_observation_ob(ob_id));
ALTER TABLE geola.tj_limite_observation_lo OWNER TO geola;

INSERT INTO geola.tj_limite_observation_lo SELECT * FROM tempa.tj_limite_observation_lo; --1155


-- Ajout du cd_code dans t_limite_lm pour jointure ce_ala et t_limite_lm car id non présent dans ce_ala

CREATE TABLE geola.alose AS (
WITH code_ce AS (
SELECT t_limite_lm.*, tr_cours_d_eau.cd_id, tr_cours_d_eau.cd_code
FROM geola.t_limite_lm
JOIN referentiel.tr_cours_d_eau
ON t_limite_lm.lm_cd_id = tr_cours_d_eau.cd_id)

SELECT DISTINCT ON(code_ce.cd_code) 
code_ce.lm_id::integer AS lm_id,
geola.ce_ala.*
FROM geola.ce_ala
LEFT JOIN code_ce
ON code_ce.cd_code = ce_ala.cd_code);

--ALTER TABLE geola.t_limite_lm DROP COLUMN lm_ob_id;



-- missing constraints after work on Jules PC (without ref)
ALTER TABLE geola.t_limite_lm ADD   CONSTRAINT t_limite_lm_cd_id_fkey FOREIGN KEY (lm_cd_id) REFERENCES referentiel.tr_cours_d_eau(cd_id);
ALTER TABLE geola.t_limite_lm ADD   CONSTRAINT t_limite_lm_bv_id_fkey FOREIGN KEY (lm_bv_id) REFERENCES referentiel.tr_bassin_versant(bv_id);
ALTER TABLE geola.t_limite_lm ADD   CONSTRAINT t_limite_lm_ic_id_fkey FOREIGN KEY (lm_ic_id) REFERENCES referentiel.tr_indice_confiance(ic_id);
ALTER TABLE geola.t_limite_lm ADD   CONSTRAINT t_limite_lm_id_roe_fkey FOREIGN KEY (lm_id_roe) REFERENCES referentiel.tr_roe(roe_id_roe);
ALTER TABLE geola.t_limite_lm ADD   CONSTRAINT t_limite_lm_ta_id_fkey FOREIGN KEY (lm_ta_id) REFERENCES referentiel.tr_taxon(ta_id);
ALTER TABLE geola.t_limite_lm ADD   CONSTRAINT t_limite_lm_co_id_fkey FOREIGN KEY (lm_co_id) REFERENCES referentiel.tr_cogepomi(co_id);
ALTER TABLE geola.t_limite_lm ADD   CONSTRAINT t_limite_lm_tl_id_fkey FOREIGN KEY (lm_tl_id) REFERENCES referentiel.tr_type_limite(tl_id);


-- checks for limits


SELECT count(*), lm_ta_id FROM geol.t_limite_lm 
WHERE geom IS null
GROUP BY lm_ta_id;

SELECT count(*), lm_ta_id FROM geol.t_limite_lm 
WHERE geom IS NOT NULL
GROUP BY lm_ta_id;

SELECT count(*), lm_ta_id, lm_presence FROM geol.t_limite_lm 
WHERE geom IS null
GROUP BY lm_ta_id, lm_presence
ORDER BY lm_presence, lm_ta_id;


-- We lost inserts during work


UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2014); --100

-- A la main sur la carte ?
WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =185)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=185 AND lm_ta_id=2014;--1

WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =157)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=157 AND lm_ta_id=2014;--1

WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =152)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=152 AND lm_ta_id=2014;--1


WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =157)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=157 AND lm_ta_id=2014;--1

-- Canal des Etangs
WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =18)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=18 AND lm_ta_id=2056;--1

WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =176)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=176 AND lm_ta_id=2014;--1

WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =179)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=179 AND lm_ta_id=2014;--1

WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =2)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=2 AND lm_ta_id!=2020;--1

WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =153)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=153 AND lm_ta_id!=2020;--1

WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =156)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=156 AND lm_ta_id!=2020;--1

WITH missing AS (
SELECT id ml_id, St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id =166)
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=166 AND lm_ta_id!=2020;--1



WITH missing AS (
SELECT id , St_StartPoint ((st_dump(ce.geom)).geom) geom 
FROM geol.ce_lpm ce
WHERE id IN (39,163,189,177,167,70,31,57,38,27))
UPDATE geol.t_limite_lm lm SET geom = missing.geom  FROM missing
WHERE lm.lm_id=id AND lm_ta_id!=2020;--10


-- rajout de limites 01/06/2023

-- erreur certains points sont dans limite saumon mais c'est celles des lamproies (vérifié sur QGIS)
SELECT * FROM geol.t_limite_lm WHERE lm_id=1343;
UPDATE geol.t_limite_lm SET lm_ta_id=2014 WHERE lm_id=1343;

SELECT * FROM geol.t_limite_lm WHERE lm_id=1345;
UPDATE geol.t_limite_lm SET lm_ta_id=2014 WHERE lm_id=1345;

SELECT * FROM geol.t_limite_lm WHERE lm_id=1293;
UPDATE geol.t_limite_lm SET lm_ta_id=2014 WHERE lm_id=1293;

SELECT * FROM geol.t_limite_lm WHERE lm_id=1354;
UPDATE geol.t_limite_lm SET lm_ta_id=2014 WHERE lm_id=1354;

SELECT * FROM geol.t_limite_lm WHERE lm_id=1317;
UPDATE geol.t_limite_lm SET lm_ta_id=2014 WHERE lm_id=1317;

SELECT * FROM geol.t_limite_lm WHERE lm_id=1339;
UPDATE geol.t_limite_lm SET lm_ta_id=2014 WHERE lm_id=1339;

SELECT * FROM geol.t_limite_lm WHERE lm_id=1340;
UPDATE geol.t_limite_lm SET lm_ta_id=2014 WHERE lm_id=1340;


--DELETE FROM geol.t_limite_lm WHERE lm_id=1434 AND lm_ta_id=2220;
--DELETE FROM geol.t_limite_lm WHERE lm_id=1542 AND lm_ta_id=2220;



SELECT * FROM geol.t_limite_lm WHERE lm_id=1307;










-- on enlève le vieux rhône
UPDATE geol.t_limite_lm lm SET geom = NULL  WHERE lm_id = 180 AND lm_ta_id=2014;--1


UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2009); --0


UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2010); --0


UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2011); --18

UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2012); --0

SELECT * FROM tempa.points_nouveaux 
EXCEPT
SELECT pn.* FROM tempa.points_nouveaux pn JOIN  geol.t_limite_lm lm ON st_intersects(lm.geom,pn.geom)

UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2055); --0

UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2056); --18

UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2057); --19

UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux pn

; 


UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux_ala pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2055);  --0

UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux_ala pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2056);  --13

UPDATE geol.t_limite_lm lm SET geom = pn.geom FROM tempa.points_nouveaux_ala pn
WHERE (lm.lm_id, lm_ta_id) = (pn.lm_id,2057);  --4






-- Searching for streams where geom is NULL and roe EXISTS 

WITH nullgeom  AS(
SELECT * FROM geol.t_limite_lm 
WHERE geom IS NULL),
notnullgeom AS (
SELECT * FROM geol.t_limite_lm 
WHERE geom IS NOT NULL)
SELECT * FROM nullgeom ng JOIN notnullgeom nng ON (ng.lm_cd_id, ng.lm_ta_id)=(nng.lm_cd_id, nng.lm_ta_id) ORDER BY ng.lm_id;
--2 un avec ROE, l'autre sans (voir plus haut modification à la main sur la carte)



-- todo KEY ON both lm_id and lm_ta_id is a mess
-- create oldid column to store the value in both tj_limite_observation_lo and t_limit_lm
ALTER TABLE geol.t_limite_lm ADD COLUMN lm_oldid TEXT;
ALTER TABLE geol.tj_limite_observation_lo ADD COLUMN lo_lm_oldid TEXT;
COMMENT ON COLUMN geol.t_limite_lm.ob_oldid IS 
'Ancien identifiant, attention, il y a des valeurs dupliquées entre saumon et les autres espèces (même id dans excel)';
COMMENT ON COLUMN geol.tj_limite_observation_lo.lo_lm_oldid IS 
'Ancien identifiant, attention, il y a des valeurs dupliquées entre saumon et les autres espèces (même id dans excel)';
UPDATE geol.t_limite_lm SET lm_oldid = lm_id||'('||lm_ta_id||')'; --947
UPDATE  geol.tj_limite_observation_lo SET lo_lm_oldid = lo_lm_id||'('||lo_ta_id||')'; --1597
ALTER TABLE geol.tj_limite_observation_lo DROP CONSTRAINT c_fk_lo_lm_id;
ALTER TABLE geol.t_limite_lm DROP CONSTRAINT c_pk_t_limite_lm;
SELECT max(lm_id) FROM geol.t_limite_lm WHERE lm_ta_id !=2020 ; --1288
CREATE TEMPORARY SEQUENCE seq;
ALTER SEQUENCE seq RESTART WITH 1289;

UPDATE geol.t_limite_lm SET lm_id=nextval('seq') WHERE lm_ta_id =2220; -- for salmo we reset the id 302
UPDATE geol.tj_limite_observation_lo SET lo_lm_id = lm_id FROM geol.t_limite_lm WHERE 
lo_lm_oldid = lm_oldid; --1597


ALTER TABLE geol.t_limite_lm ADD CONSTRAINT c_pk_t_limite_lm_lm_id PRIMARY KEY (lm_id);
ALTER TABLE geol.t_limite_lm ADD CONSTRAINT c_uk_t_limite_lm_lm_oldid UNIQUE (lm_oldid);
ALTER TABLE geol.tj_limite_observation_lo ADD CONSTRAINT c_fk_t_limite_lm_lm_oldid FOREIGN KEY (lo_lm_oldid) REFERENCES geol.t_limite_lm (lm_oldid);
ALTER TABLE geol.tj_limite_observation_lo ADD CONSTRAINT c_fk_lo_lm_id FOREIGN KEY (lo_lm_id) REFERENCES geol.t_limite_lm (lm_id);




-- TODO remettre les coordonnées lm_xl93_2154 lm_yl93_2154
UPDATE geol.t_limite_lm SET (lm_xl93_2154, lm_yl93_2154 ) = (st_x(geom), st_y(geom)); --947


-- Get missing limits from the table observation
WITH missing_point_from_ob AS (
SELECT lm_id, ob.geom FROM geol.t_limite_lm lm
JOIN  geol.tj_limite_observation_lo AS tlol ON lo_lm_id=lo_lm_id 
JOIN  geol.t_observation_ob AS ob ON lo_ob_id = ob_id
WHERE lm.geom IS NULL AND ob.geom IS NOT NULL)
UPDATE geol.t_limite_lm SET geom=missing_point_from_ob.geom FROM missing_point_from_ob



WHERE missing_point_from_ob.lm_id=t_limite_lm.lm_id; --338


-- TODO corriger l'encodage lm_nom_expert , lm_commentaire

-- A partir de limits les lm_id sont inchangés

SELECT tru.lm_commentaire,lm.lm_commentaire,tru.lm_nom_expert,lm.lm_nom_expert FROM tempa.temp_correct_utf8 tru
JOIN geol.t_limite_lm AS lm ON lm.lm_id=tru.lm_id; 

UPDATE geol.t_limite_lm lm SET (lm_commentaire,lm_nom_expert)=(tru.lm_commentaire,tru.lm_nom_expert) FROM tempa.temp_correct_utf8 tru
WHERE lm.lm_id=tru.lm_id; --644

-- A partir de sat

SELECT tru.lm_commentaire,lm.lm_commentaire,tru.lm_nom_expert,lm.lm_nom_expert FROM tempa.temp_correct_utf8_salmo tru
JOIN geol.t_limite_lm AS lm ON lm.lm_oldid=tru.lm_id||'(2220)'; 

UPDATE geol.t_limite_lm lm SET lm_commentaire=tru.lm_commentaire FROM tempa.temp_correct_utf8 tru
WHERE lm.lm_oldid=tru.lm_id||'(2220)'; --284


-- récupération des cours d'eaux de chaque bassins versant 

ALTER TABLE referentiel.tr_bassin_versant ADD COLUMN bv_cd_id INTEGER;
ALTER TABLE referentiel.tr_bassin_versant ADD CONSTRAINT c_fk_bv_cd_id FOREIGN KEY
 (bv_cd_id) REFERENCES referentiel.tr_cours_d_eau(cd_id);  

 


DROP VIEW IF EXISTS referentiel.cebv ;
CREATE MATERIALIZED VIEW referentiel.cebv AS (
WITH joince AS(
    SELECT  * FROM referentiel.tr_bassin_versant AS tbv 
    JOIN referentiel.tr_cours_d_eau AS tcde 
    ON st_intersects(tcde.cd_geom,tbv.bv_geom)
    WHERE cd_classe = '1'),
ce_per_bv AS (
    SELECT bv_id, st_multi(ST_Union(cd_geom)) AS geom, 
    bv_libelle FROM joince GROUP BY bv_libelle,bv_id
)
 SELECT * FROM ce_per_bv);--112
ALTER MATERIALIZED VIEW referentiel.cebv OWNER TO geola;



-- 02/06 Cédric fix switched values on oldid


SELECT * FROM geol.t_limit_lmsauv ls
JOIN geol.t_limite_lm l
ON l.lm_id = ls.lm_id;
-- pas de problème ?


SELECT * FROM geol.t_limite_lm 
JOIN geol.tj_limite_observation_lo AS tjol ON tjol.lo_lm_id=lm_id
JOIN geol.t_observation_ob AS ob ON ob.ob_id=lo_ob_id
JOIN referentiel.tr_bassin_versant ON bv_id = ob_bv_id
WHERE lm_ta_id = 2057

CREATE TABLE tempa.t_limit_lmsauv AS(
SELECT
lm_id,
ob.ob_id AS lm_ob_id,
lm_annee_debut,
lm_anne_fin,
lm_presence,
cours_eau_entier AS lm_cours_eau_entier,
lm_nom_expert,
lm_date_expert,
cd_id AS lm_cd_id,
id_bv AS lm_bv_id,
lm_tl_id,
lm_roe_id AS lm_id_roe,
lm_ic_id,
ta_id AS lm_ta_id, 
co_id AS lm_co_di,
commentaires AS lm_commentaire,
'2019-01-01' AS lm_datelastupdate,
st_x(ob.geom) AS lm_xl93_2154,
st_y(ob.geom) AS lm_yl93_2154,
ob.geom 
FROM tempa.t_limit_lm 
LEFT JOIN (SELECT DISTINCT ON (ob_id_sig) * FROM geol.t_observation_ob ) ob 
ON ob.ob_id_sig=id_sig::INTEGER
JOIN referentiel.tr_cours_d_eau cd ON cd.cd_code = lm_cd_id);--644



SELECT * FROM tempa.t_limit_lm lm JOIN geol.t_limite_lm lm2 ON ST_intersects(lm.geom,lm2.geom)

-- les geométries sont au mauvais endroit
-- on était sensé allé les chercher à partir des données de la table observation 
-- lets redo that

-- the following gives a point at Iffezheim
WITH newlimit AS (
SELECT lm_id, lm_oldid, ob.geom AS obgeom, lm.geom FROM geol.t_limite_lm lm
JOIN geol.tj_limite_observation_lo AS tjol ON tjol.lo_lm_id=lm_id
JOIN geol.t_observation_ob AS ob ON ob.ob_id=lo_ob_id
WHERE ob.geom IS NOT NULL
ORDER BY lm_id)

SELECT obgeom FROM newlimit WHERE lm_id=20

-- On commence par mettre a NULL les geom qui n'ont pas d'ID sig

WITH efface_tout AS (
SELECT g.*, l.id_sig 
FROM geol.t_limite_lm  g 
JOIN tempa.t_limit_lm l ON l.lm_id = g.lm_id
WHERE id_sig IS NULL)

UPDATE geol.t_limite_lm SET geom=NULL FROM efface_tout
WHERE efface_tout.lm_id = t_limite_lm.lm_id; --546

-- On va chercher les géomtries des ouvrages si présents

WITH barrages_geom AS (
SELECT lm_id, roe_geom FROM geol.t_limite_lm  g 
JOIN referentiel.tr_roe AS tr ON roe_id_roe=lm_id_roe 
WHERE  geom IS NULL)   --284
UPDATE geol.t_limite_lm SET geom=roe_geom FROM barrages_geom
WHERE barrages_geom.lm_id = t_limite_lm.lm_id; --284


SELECT * FROM geol.t_limite_lm  g WHERE geom IS NULL; --262

-- Corrections of geom


WITH newlimit AS (
SELECT lm_id, lm_oldid, ob_id, ob.geom AS obgeom, lm.geom,lm FROM 
geol.t_limite_lm lm
JOIN geol.tj_limite_observation_lo AS tjol ON tjol.lo_lm_id=lm_id
JOIN geol.t_observation_ob AS ob ON ob.ob_id=lo_ob_id
WHERE ob.geom IS NOT NULL AND lm.geom IS NULL
ORDER BY lm_id)
--SELECT DISTINCT ON (lm_id, obgeom) * FROM newlimit  
UPDATE geol.t_limite_lm lm SET geom = obgeom FROM newlimit WHERE 
newlimit.lm_id=lm.lm_id AND lm.lm_id != 419; --14


-- 4 points sur l'Adour on prend le plus amont.
WITH newlimit AS (
SELECT lm_id, lm_oldid, ob_id, ob.geom AS obgeom, lm.geom,lm FROM 
geol.t_limite_lm lm
JOIN geol.tj_limite_observation_lo AS tjol ON tjol.lo_lm_id=lm_id
JOIN geol.t_observation_ob AS ob ON ob.ob_id=lo_ob_id
WHERE ob.geom IS NOT NULL AND lm.geom IS NULL
ORDER BY lm_id)
--SELECT DISTINCT ON (lm_id, obgeom) * FROM newlimit  
UPDATE geol.t_limite_lm lm SET geom = obgeom FROM newlimit WHERE 
newlimit.lm_id=lm.lm_id AND ob_id = 1629; --1


UPDATE geol.t_limite_lm SET (lm_presence, lm_commentaire) =
(FALSE, 'valeur récupérée depuis la carte des limites') 
WHERE lm_nom_expert = 'valeur manquante'
AND lm_cours_eau_entier =FALSE
AND lm_commentaire IS NULL; -- 205


UPDATE geol.t_limite_lm SET (lm_presence, lm_commentaire) =
(FALSE, 'Valeur récupérée depuis le ROE') 
WHERE lm_nom_expert = 'valeur manquante'
AND lm_cours_eau_entier =FALSE
AND lm_commentaire ='valeur récupérée depuis la carte des limites'
AND lm_id_roe IS NOT NULL
; -- 121

-- TRAVAIL SUR SAUMONS
SELECT regexp_replace(lm_oldid, '\(2220\)','') FROM geol.t_limite_lm

WITH efface_tout AS (
SELECT g.*, l.id_sig FROM geol.t_limite_lm  g 
JOIN tempa.t_limit_lm l ON l.lm_id = regexp_replace(lm_oldid, '\(2220\)','')::integer
WHERE id_sig IS NULL AND g.lm_ta_id = 2220)

UPDATE geol.t_limite_lm SET (geom, lm_commentaire)=
(NULL,
CASE WHEN efface_tout.lm_commentaire = 'Valeur récupérée depuis le ROE' THEN NULL
WHEN efface_tout.lm_commentaire = 'valeur récupérée depuis la carte des limites' THEN NULL
ELSE efface_tout.lm_commentaire END) FROM efface_tout
WHERE efface_tout.lm_id = t_limite_lm.lm_id; --231

-- On va chercher les géométries des ouvrages si présents

WITH barrages_geom AS (
SELECT lm_id, roe_geom FROM geol.t_limite_lm  g 
JOIN referentiel.tr_roe AS tr ON roe_id_roe=lm_id_roe 
WHERE  geom IS NULL AND g.lm_ta_id = 2220)   
UPDATE geol.t_limite_lm SET geom=roe_geom FROM barrages_geom
WHERE barrages_geom.lm_id = t_limite_lm.lm_id; --112


SELECT * FROM geol.t_limite_lm  g WHERE geom IS NULL; --366

-- Corrections of geom


WITH newlimit AS (
SELECT lm_id, lm_oldid, ob_id, ob.geom AS obgeom, lm.geom,lm FROM 
geol.t_limite_lm lm
JOIN geol.tj_limite_observation_lo AS tjol ON tjol.lo_lm_id=lm_id
JOIN geol.t_observation_ob AS ob ON ob.ob_id=lo_ob_id
WHERE lm_ta_id = 2220
AND ob.geom IS NOT NULL AND lm.geom IS NULL

ORDER BY lm_id)
-- Aucune données depuis la table observation pour le saumon 


-- On supprime les géométries des limites saumon qui ont été générées depuis la carte des limites 

UPDATE  geol.t_limite_lm AS tll 
SET geom = NULL
WHERE lm_commentaire = 'valeur récupérée depuis la carte des limites'
AND lm_ta_id = 2220; --18

--pour les autres espèces
SELECT *
FROM geol.t_limite_lm AS tll
WHERE lm_commentaire = 'valeur récupérée depuis la carte des limites'
AND lm_ta_id IN (2011, 2014, 2055, 2056, 2057) --1 seule ligne - ATTENTION incohérence entre lm_ta_id(LPM) et lm_oldid(SAT)


SELECT * FROM  geol.tj_limite_observation_lo WHERE lo_lm_id= 1354;
DELETE FROM geol.tj_limite_observation_lo WHERE lo_lm_id= 1354;
DELETE FROM geol.t_limite_lm WHERE lm_id=1354;
DELETE FROM geol.tj_limite_observation_lo WHERE lo_lm_id 
IN (1317,1340,1345,1293,1343,1339);
DELETE FROM geol.t_limite_lm WHERE lm_id
IN (1317,1340,1345,1293,1343,1339);
--On recherche si d'autre ligne avec incohérence entre les lm_ta_id et le lm_oldid sur le code espèce

SELECT lm_id,lm_ta_id,substring(regexp_replace(lm_oldid, '.*\(',''),1,4) as oldid
FROM geol.t_limite_lm 
where lm_ta_id!=substring(regexp_replace(lm_oldid, '.*\(',''),1,4)::integer

UPDATE  geol.t_limite_lm AS tll 
SET geom = NULL
WHERE lm_commentaire = 'valeur récupérée depuis la carte des limites'
AND lm_ta_id IN (2011, 2014, 2055, 2056, 2057); --1

--Pour le saumon
WITH code_ce AS (
SELECT t_limite_lm.*, tr_cours_d_eau.cd_id, tr_cours_d_eau.cd_code
FROM geol.t_limite_lm
JOIN referentiel.tr_cours_d_eau
ON t_limite_lm.lm_cd_id = tr_cours_d_eau.cd_id
WHERE lm_ta_id = 2220),

join_ce_sat AS (
SELECT DISTINCT ON(code_ce.cd_code) 
code_ce.lm_id::integer AS lm_id,
geol.ce_sat.geom,
code_ce.geom AS geom_limit
FROM geol.ce_sat
LEFT JOIN code_ce
ON code_ce.cd_code = ce_sat.cd_code),

jointure_finale AS (
SELECT lm_id, geom_limit, St_StartPoint ((st_dump(geom)).geom) newgeom FROM join_ce_sat),

les_deux AS (
SELECT * FROM jointure_finale 
WHERE lm_id IS NOT NULL 
AND (geom_limit && newgeom OR geom_limit IS NULL))

UPDATE geol.t_limite_lm SET geom = newgeom FROM 
les_deux WHERE les_deux.lm_id = t_limite_lm.lm_id;-- 118
--st_distance(geom_limit, newgeom) > 200 

--pour la LPM pas de géométrie à corriger

WITH code_ce AS (
SELECT t_limite_lm.*, tr_cours_d_eau.cd_id, tr_cours_d_eau.cd_code
FROM geol.t_limite_lm
JOIN referentiel.tr_cours_d_eau
ON t_limite_lm.lm_cd_id = tr_cours_d_eau.cd_id
WHERE lm_ta_id = 2014),

join_ce_lpm AS (
SELECT DISTINCT ON(code_ce.cd_code) 
code_ce.lm_id::integer AS lm_id,
geol.ce_lpm.geom,
code_ce.geom AS geom_limit
FROM geol.ce_lpm
LEFT JOIN code_ce
ON code_ce.cd_code = ce_lpm.cd_code),

jointure_finale AS (
SELECT lm_id, geom_limit, St_StartPoint ((st_dump(geom)).geom) newgeom FROM join_ce_lpm),

les_deux AS (
SELECT * FROM jointure_finale 
WHERE lm_id IS NOT NULL 
AND (geom_limit && newgeom OR geom_limit IS NULL))

select * from les_deux where st_distance(geom_limit, newgeom) > 200 order by lm_id
UPDATE geol.t_limite_lm SET geom = newgeom FROM 
les_deux WHERE les_deux.lm_id = t_limite_lm.lm_id;-- 1






-- Avec le buffer 0 lignes
WITH badly_projected AS (
SELECT lm_id, lm_ta_id, lm_bv_id, geom  FROM geol.t_limite_lm lm 
WHERE geom IS NOT NULL 
AND lm_bv_id IS NOT NULL
EXCEPT
SELECT lm_id, lm_ta_id, lm_bv_id, geom  FROM geol.t_limite_lm lm
JOIN referentiel.tr_bassin_versant 
ON bv_id=lm_bv_id
WHERE st_intersects(st_buffer(geom, 10000), bv_geom))

SELECT * FROM  geol.t_limite_lm WHERE lm_id IN (SELECT lm_id FROM badly_projected)



SELECT st_intersects(st_buffer(geom, 10000), bv_geom) 
FROM geol.t_limite_lm lm
JOIN referentiel.tr_bassin_versant ON bv_id=lm_bv_id
WHERE lm_id = 145










