

DROP TABLE IF EXISTS montepomi.joined_surface_err;
CREATE TABLE montepomi.joined_surface_err AS (  
        
WITH intersected AS (
SELECT serr.id_commun,
        surf_err,
        bu_idsegment, 
        date,
        ce_nom,
        st_intersection(serr.geom, bu.geom) AS geomsplit       
        FROM montepomi.surface_err_loire_2024 serr
        JOIN france.basinunit_bu bu ON bu.geom && serr.geom
UNION
        SELECT serr.id_commun,
        surf_err,
        bu_idsegment, 
        date, 
        ce_nom,
        st_intersection(serr.geom, bu.geom) AS geomsplit       
        FROM montepomi.surface_err_bretagne_2024 serr
        JOIN france.basinunit_bu bu ON bu.geom && serr.geom) 
        
SELECT id_commun,
        bu_idsegment AS idsegment,
        st_length(geomsplit)/sum(st_length(geomsplit)) OVER (PARTITION BY id_commun) * surf_err AS surf_err,
        date,
        ce_nom,
        geomsplit AS geom
       FROM intersected
       WHERE NOT ST_IsEmpty(geomsplit));--60656
 
/*
 tout pareil

 SELECT sum(sum) FROM (
   SELECT sum(surf_err) FROM montepomi.surface_err_loire_2024  
   UNION
   SELECT sum(surf_err) FROM montepomi.surface_err_bretagne_2024 ) toto  
   
   
SELECT sum(surf_err) FROM montepomi.joined_surface_err
 */
        
        

 