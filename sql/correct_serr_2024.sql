-- This script will be used to fix data in sql
-- but manual edition will be needed using limits, dam localtion and checking that we have not i
-- added serr surface where no salmon are present

-- get data from laetitia to DB

-- les couches sont
montepomi.hist_sat_18_siecle
montepomi.bgmlinsatcolonise2020
france.rns

SELECT * FROM france.rns

ALTER TABLE montepomi.bgmlinsatcolonise2020
  ALTER COLUMN geom 
  TYPE Geometry(MULTILINESTRING, 3035) 
  USING ST_Transform(geom, 3035);
  
ALTER TABLE montepomi.hist_sat_18_siecle
  ALTER COLUMN geom 
  TYPE Geometry(MULTILINESTRING, 3035) 
  USING ST_Transform(geom, 3035);

DROP TABLE montepomi.tempbuff;
CREATE TABLE montepomi.tempbuff AS (SELECT id ,st_buffer(geom,50,'endcap=flat join=bevel') AS geom FROM montepomi.hist_sat_18_siecle)
  
-- buffer without extension either at bending or endcap

-- jointure à l'aide d'un buffer de 50 m autour de la couche des cours d'eaux historiques de BGM
-- on garde les segments dont plus de 10 % du linéaire intersecte le buffer

CREATE TABLE montepomi.temp_join_hist_sat_18_siecle AS (
WITH buff AS (
SELECT id ,st_buffer(geom,50,'endcap=flat join=bevel') AS geom FROM montepomi.hist_sat_18_siecle) ,

rnbtret AS (
SELECT rn.geom,  rns.* FROM france.rn JOIN france.rns ON rns.idsegment= rn.idsegment
JOIN france.rna ON rna.idsegment = rn.idsegment, buff
WHERE emu = 'FR_Bret'
AND st_intersects(buff.geom, rn.geom)),

Joined AS(
SELECT rn.idsegment,
rn.geom, 
st_length(st_intersection( b.geom, rn.geom))/st_length(rn.geom)  AS ll
FROM buff b
JOIN rnbtret rn ON st_intersects(b.geom, rn.geom) 
WHERE b.geom  && rn.geom
AND
st_length(st_intersection( b.geom, rn.geom))/st_length(rn.geom) > 0.1
)

SELECT * FROM Joined 
)--2012

-- on met à jour la couche source (rns) à partir des nouvelles carto
SELECT * FROM france.rns LIMIT 10;
UPDATE france.rns SET presencesaumon = 0 WHERE presencesaumon = 2 AND region = 'Bretagne'; --651
UPDATE france.rns SET presencesaumon = 2 WHERE idsegment IN (SELECT idsegment FROM montepomi.temp_join_hist_sat_18_siecle); --1498
UPDATE france.rns SET presencesaumon = 2  WHERE idsegment = 'FR209784';


CREATE TABLE montepomi.temp_join_bgmlinsatcolonise2020 AS (
WITH buff AS (
SELECT id ,st_buffer(geom,50,'endcap=flat join=bevel') AS geom FROM montepomi.bgmlinsatcolonise2020) ,

rnbtret AS (
SELECT rn.geom,  rns.* FROM france.rn JOIN france.rns ON rns.idsegment= rn.idsegment
JOIN france.rna ON rna.idsegment = rn.idsegment, buff
WHERE emu = 'FR_Bret'
AND st_intersects(buff.geom, rn.geom)),

Joined AS(
SELECT rn.idsegment,
rn.geom, 
st_length(st_intersection( b.geom, rn.geom))/st_length(rn.geom)  AS ll
FROM buff b
JOIN rnbtret rn ON st_intersects(b.geom, rn.geom) 
WHERE b.geom  && rn.geom
AND
st_length(st_intersection( b.geom, rn.geom))/st_length(rn.geom) > 0.1
)

SELECT * FROM Joined 
);--2012


UPDATE france.rns SET presencesaumon = 0 WHERE presencesaumon = 1 AND region = 'Bretagne'; --26
UPDATE france.rns SET presencesaumon = 1 WHERE idsegment IN (SELECT idsegment FROM montepomi.temp_join_bgmlinsatcolonise2020); --1037


--L'aval de l'Aulne n'est pas un habitat à saumons actuellement
UPDATE france.rns SET presencesaumon = 2 WHERE idsegment IN (SELECT dbeel_rivers.downstream_segments_rn('FR203280'));--38
-- L'estuaire de l'Aulne n'est pas un habitat à Saumon
UPDATE france.rns SET presencesaumon = 0 WHERE idsegment IN (SELECT dbeel_rivers.downstream_segments_rn('FR203345'));--12

-- On va pas chercher tous les estuaires, on utilise la couche wise
WITH rnbret AS (
SELECT rn.idsegment, geom  FROM france.rn JOIN france.rna ON rn.idsegment = rna.idsegment WHERE emu='FR_Bret'),

tropsale AS (
SELECT rnbret.* FROM
rnbret JOIN
european_wise2008."MasseDEauTransition_VRAP2016" me
ON st_intersects(st_transform(me.geom, 3035), rnbret.geom))

UPDATE france.rns SET presencesaumon = 0 WHERE idsegment IN (SELECT idsegment FROM tropsale); --372



