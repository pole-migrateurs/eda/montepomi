-- script à lancer après avoir téléchargé et restauré le script ROE
-- voir montepomi.sh

ALTER TABLE referentiel.tr_roe RENAME TO tr_roe_old;
CREATE TABLE referentiel.tr_roe (LIKE referentiel.tr_roe_old);
ALTER TABLE referentiel.tr_roe ALTER COLUMN roe_etat_nom TYPE TEXT;
ALTER TABLE referentiel.tr_roe ALTER COLUMN roe_type_nom TYPE TEXT;
ALTER TABLE referentiel.tr_roe ALTER COLUMN roe_id_roe TYPE TEXT;
ALTER TABLE referentiel.tr_roe ALTER COLUMN roe_chute_et TYPE TEXT;
ALTER TABLE referentiel.tr_roe ALTER COLUMN roe_nom_sec TYPE TEXT;
CREATE TEMPORARY SEQUENCE seq;
ALTER SEQUENCE seq RESTART WITH 1;
INSERT INTO referentiel.tr_roe
(roe_id, roe_id_roe, roe_statut_nom, roe_etat_code, roe_etat_nom, roe_nom_princ, roe_nom_sec, roe_type_nom, roe_chute_et, roe_geom)
SELECT nextval ('seq') AS roe_id,
cdobstecou AS roe_id_roe,
lbmodevali AS roe_statut_nom,
cdetouvrag AS roe_etat_code,
lbetouvrag AS roe_etat_nom,
nomprincip AS roe_nom_princ,
nomseconda AS roe_nom_sec,
lbtypeouvr AS roe_type_nom,
hautchutet AS roe_chute_et,
geom AS roe_geom
FROM temp.roe; --111339

