
-- tr_cogepomi ------------------------------

SELECT r.co_id, r.co_libelle, p.co_id, p.co_libelle 
FROM referentiel.tr_cogepomi r 
JOIN public.tr_cogepomi p ON r.co_id=p.co_id
WHERE r.co_libelle != p.co_libelle;

-- public.tr_bassin_versant -------------------------


SELECT r.bv_id, r.bv_libelle, p.bv_id, p.bv_libelle 
FROM referentiel.tr_bassin_versant r 
JOIN public.tr_bassin_versant p ON r.bv_id=p.bv_id
WHERE r.bv_libelle != p.bv_libelle;