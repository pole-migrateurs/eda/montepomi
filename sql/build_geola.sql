/*
 * 
 * Les tables tj_obs_cara_pop et tj_limite_observation sont des tables de jointure
 * qui n'ont jamais été utilisées par la suite. Le drop lancé sur la table observation
 * va supprimer les valeurs de ces tables.
 * TODO VIRER les deux tables vides
 */



ALTER ROLE GEOLA WITH LOGIN;

DROP TABLE geola.observation CASCADE;
DROP TABLE public.tj_limite_observation;
DROP TABLE public.tj_obs_cara_pop;



-- pour lancer le script ci dessous, les clés étrangères doivent pointer vers
-- des colonnes de clé primaire dans les tables référentielles.
-- Quand on a joué avec ROE on a oublié de remettre les constraintes de Primary key.

ALTER TABLE referentiel.tr_roe ADD CONSTRAINT c_pk_roe_id PRIMARY KEY (roe_id);


-- certaines colonnes du fichier excel correspondent (bien naturellement)
-- au nom et pas à l'identifiant.
-- ci dessous on va récupérer l'id à partir du texte


insee => cm_code (ob_cm_id)
code_hydro => ob_cd_id
A ajouter => 
bv_id
observation_coralie
nom_bassin_versant

-- pas de valeurs manquante  ---------------------------------

SELECT ob_cm_id, insee FROM "temp".observation  o 
WHERE o.insee IS NOT NULL 
AND o.ob_cm_id IS NULL;

SELECT ob_cd_id, code_hydro  FROM "temp".observation  o 
WHERE o.code_hydro IS NOT NULL 
AND o.ob_cd_id IS NULL; --00


SELECT bv_id, nom_bassin_versant  FROM "temp".observation  o 
WHERE o.nom_bassin_versant IS NOT NULL 
AND o.bv_id IS NULL; --00

-- On ne garde pas la colonne obb_coordonnees qui sert à rien

SELECT * FROM referentiel.tr_cours_d_eau WHERE cd_code = 'ind';
UPDATE referentiel.tr_cours_d_eau SET cd_code = 'gir' WHERE cd_id = 125783;
UPDATE referentiel.tr_cours_d_eau SET cd_code = 'brou' WHERE cd_id = 125786;
SELECT * FROM "temp".observation where code_hydro ='ind';
UPDATE "temp".observation SET code_hydro = 'brou' WHERE ob_cd_id = 'Canal de Broue'; --2
UPDATE "temp".observation SET code_hydro = 'gir' WHERE ob_cd_id = 'Gironde'; --12

UPDATE "temp".observation  set ob_nombre=NULL WHERE ob_nombre ILIKE '%plus %';
UPDATE "temp".observation
  SET ob_poids='8360'
  WHERE id_bdd=2573.0;
UPDATE "temp".observation
  SET ob_poids='1935'
  WHERE id_bdd=2574.0;
UPDATE "temp".observation
  SET ob_poids='13460'
  WHERE id_bdd=2575.0;
UPDATE "temp".observation
  SET ob_poids='7711'
  WHERE id_bdd=2576.0;
UPDATE "temp".observation
  SET ob_poids='2595'
  WHERE id_bdd=2571.0;
UPDATE "temp".observation
  SET ob_poids='1140'
  WHERE id_bdd=2572.0;


ALTER TABLE referentiel.tr_cours_d_eau ADD CONSTRAINT c_uk_cd_code UNIQUE (cd_code);
ALTER TABLE referentiel.tr_roe ADD CONSTRAINT c_uk_roe_id_roe UNIQUE (roe_id_roe);
ALTER TABLE referentiel.tr_region_hydrographique ADD CONSTRAINT c_uk_rh_code UNIQUE (rh_code);
ALTER TABLE referentiel.tr_secteur_hydrographique ADD CONSTRAINT c_uk_sh_code UNIQUE (sh_code);
ALTER TABLE referentiel.tr_sous_secteur_hydrographique ADD CONSTRAINT c_uk_ss_code UNIQUE (ss_code);
ALTER TABLE referentiel.tr_zone_hydrographique ADD CONSTRAINT c_uk_zh_code UNIQUE (zh_code);
ALTER TABLE referentiel.tr_departement ADD CONSTRAINT c_uk_de_code UNIQUE (de_code);

DROP TABLE IF EXISTS geola.t_observation_ob;
CREATE TABLE geola.t_observation_ob (
  ob_id int4 NOT NULL,
  ob_id_sig int4 NULL,
  ob_annee_debut int4,
  ob_annee_fin int4,
  ob_presence bool NOT NULL DEFAULT true,
  ob_coordonnees text NULL,
  ob_nombre int4 NULL,
  ob_poids float4 NULL,
  ob_comment_obs text NULL,
  ob_comment_lieu text NULL,
  ob_comment_abond text NULL,
  ob_bv_id int4 NULL,
  ob_rh_code TEXT NULL,
  ob_sh_code TEXT NULL,
  ob_ss_code TEXT NULL,
  ob_zh_code TEXT NULL,
  ob_cd_code TEXT NULL,
  ob_de_code TEXT NULL,
  ob_cm_id int4 NULL,
  ob_no_id int4,
  ob_to_id int4,
  ob_ro_id int4,
  ob_ce_id int4,
  ob_ic_id int4 NOT NULL,
  ob_co_id int4 NULL, -- cogepomi
  ob_roe_code TEXT NULL,
  ob_codehydro TEXT NULL,
  ob_ta_id int4 NOT NULL, 
  ob_commentinsert varchar NULL,
  ob_lastupdate date DEFAULT '2019-01-01',
  geom geometry(point, 2154) DEFAULT NULL,
  CONSTRAINT observation_pkey PRIMARY KEY (ob_id),
  CONSTRAINT observation_ob_bv_id_fkey FOREIGN KEY (ob_bv_id) REFERENCES referentiel.tr_bassin_versant(bv_id),
  CONSTRAINT observation_ob_codehydro_id_fkey FOREIGN KEY (ob_codehydro) REFERENCES referentiel.tr_cours_d_eau(cd_code),
  CONSTRAINT observation_ob_ce_id_fkey FOREIGN KEY (ob_ce_id) REFERENCES referentiel.tr_condition_environnementale(ce_id),
  CONSTRAINT observation_ob_cm_id_fkey FOREIGN KEY (ob_cm_id) REFERENCES referentiel.tr_commune(cm_id),
  CONSTRAINT observation_ob_de_code_fkey FOREIGN KEY (ob_de_code) REFERENCES referentiel.tr_departement(de_code),
  CONSTRAINT observation_ob_ic_id_fkey FOREIGN KEY (ob_ic_id) REFERENCES referentiel.tr_indice_confiance(ic_id),
  CONSTRAINT observation_ob_no_id_fkey FOREIGN KEY (ob_no_id) REFERENCES referentiel.tr_nature_observation(no_id),
  CONSTRAINT observation_ob_rh_id_fkey FOREIGN KEY (ob_rh_code) REFERENCES referentiel.tr_region_hydrographique(rh_code),
  CONSTRAINT observation_ob_roe_code_fkey FOREIGN KEY (ob_roe_code) REFERENCES referentiel.tr_roe(roe_id_roe),
  CONSTRAINT observation_ob_sh_code_fkey FOREIGN KEY (ob_sh_code) REFERENCES referentiel.tr_secteur_hydrographique(sh_code),
  CONSTRAINT observation_ob_ss_code_fkey FOREIGN KEY (ob_ss_code) REFERENCES referentiel.tr_sous_secteur_hydrographique(ss_code),
  CONSTRAINT observation_ob_cd_code_fkey FOREIGN KEY (ob_cd_code) REFERENCES referentiel.tr_cours_d_eau(cd_code),
  CONSTRAINT observation_ob_ta_id_fkey FOREIGN KEY (ob_ta_id) REFERENCES referentiel.tr_taxon(ta_id),
  CONSTRAINT observation_ob_to_id_fkey FOREIGN KEY (ob_to_id) REFERENCES referentiel.tr_type_observation(to_id),
  CONSTRAINT observation_ob_zh_code_fkey FOREIGN KEY (ob_zh_code) REFERENCES referentiel.tr_zone_hydrographique(zh_code),
  CONSTRAINT observation_ob_ro_id FOREIGN KEY (ob_ro_id) REFERENCES referentiel.tr_reference_observation(ro_id)
  );
 
-- Ajout en commentaire des noms de toutes les colonnes 
COMMENT ON COLUMN geola.t_observation_ob.ob_id IS 'identifiant de l''observation';
COMMENT ON COLUMN geola.t_observation_ob.ob_no_id IS 'identifiant de la nature de l''observation, e.g. juvénile vivant, adulte mort, nid ...';
COMMENT ON COLUMN geola.t_observation_ob.ob_to_id IS 'identifiant du type l''observation, e.g. pêche scientifique . ...';
-- COMMENT ON COLUMN geola.t_observation_ob.ob_id_sig IS --voir données de SIG
COMMENT ON COLUMN geola.t_observation_ob.ob_annee_debut IS 'Année de début de l''observation';
COMMENT ON COLUMN geola.t_observation_ob.ob_annee_fin IS 'Année de fin de l''observation';
COMMENT ON COLUMN geola.t_observation_ob.ob_presence IS 'Présence des poissons migrateurs';
COMMENT ON COLUMN geola.t_observation_ob.ob_coordonnees IS 'Coordonnées des observations'; --seulement 1 ligne
COMMENT ON COLUMN geola.t_observation_ob.ob_nombre IS 'Nombre d''observations';
COMMENT ON COLUMN geola.t_observation_ob.ob_poids IS 'Poids (en kg)';
COMMENT ON COLUMN geola.t_observation_ob.ob_comment_obs IS 'Commentaire sur l''observation';
COMMENT ON COLUMN geola.t_observation_ob.ob_comment_lieu IS 'Commentaire sur le lieu';
COMMENT ON COLUMN geola.t_observation_ob.ob_comment_abond IS 'Commentaire sur l''andondance';
COMMENT ON COLUMN geola.t_observation_ob.ob_bv_id IS 'Identifiant du bassin versant';
COMMENT ON COLUMN geola.t_observation_ob.ob_rh_code IS 'Code de la région hydrographique';
COMMENT ON COLUMN geola.t_observation_ob.ob_sh_code IS 'Code du secteur hydrographique';
COMMENT ON COLUMN geola.t_observation_ob.ob_ss_code IS 'Code du sous-secteur hydrographique';
COMMENT ON COLUMN geola.t_observation_ob.ob_zh_code IS 'Code de la zone hydrographique';
COMMENT ON COLUMN geola.t_observation_ob.ob_cd_code IS 'Code du cours d''eau';
COMMENT ON COLUMN geola.t_observation_ob.ob_de_code IS 'Code du département';
COMMENT ON COLUMN geola.t_observation_ob.ob_cm_id IS 'Identifiant de la commune';
COMMENT ON COLUMN geola.t_observation_ob.ob_ro_id IS 'Reférence de l''observation';
COMMENT ON COLUMN geola.t_observation_ob.ob_ce_id IS 'Identifiant de la condition environnementale';
COMMENT ON COLUMN geola.t_observation_ob.ob_ic_id IS 'Indice de confiance de l''observation';
COMMENT ON COLUMN geola.t_observation_ob.ob_co_id IS 'Identifiant COGEPOMI'; --??
COMMENT ON COLUMN geola.t_observation_ob.ob_roe_code IS 'Code ROE';
COMMENT ON COLUMN geola.t_observation_ob.ob_codehydro IS 'Code du cours d''eau';
COMMENT ON COLUMN geola.t_observation_ob.ob_ta_id IS 'Code du taxon';
COMMENT ON COLUMN geola.t_observation_ob.ob_commentinsert IS 'Commentaires (provenant de la colonne supplémentaire commentaire coralie)';
COMMENT ON COLUMN geola.t_observation_ob.ob_lastupdate IS 'Date de la dernière modification, les données préparées par le pôle avant 2023 sont marquées 2019-01-01';
COMMENT ON COLUMN geola.t_observation_ob.geom IS 'geometrie (point) des observations';


 GRANT ALL ON ALL TABLES IN SCHEMA referentiel to geola;
 GRANT ALL ON ALL TABLES IN SCHEMA geola to geola;
 ALTER TABLE geola.t_observation_ob OWNER TO geola;

ALTER TABLE referentiel.tr_bassin_versant OWNER TO geola;
ALTER TABLE referentiel.tr_classe_abondance OWNER TO geola;
ALTER TABLE referentiel.tr_cogepomi OWNER TO geola;
ALTER TABLE  referentiel.tr_commune OWNER TO geola;
ALTER TABLE  referentiel.tr_condition_environnementale OWNER TO geola;
ALTER TABLE  referentiel.tr_cours_d_eau OWNER TO geola;
ALTER TABLE  referentiel.tr_departement OWNER TO geola;
ALTER TABLE  referentiel.tr_indice_confiance OWNER TO geola;
ALTER TABLE referentiel.tr_nature_observation OWNER TO geola;
ALTER TABLE referentiel.tr_reference_observation OWNER TO geola;
ALTER TABLE  referentiel.tr_region_hydrographique OWNER TO geola;
ALTER TABLE  referentiel.tr_roe OWNER TO geola;
ALTER TABLE  referentiel.tr_roe_old OWNER TO geola;
ALTER TABLE  referentiel.tr_secteur_hydrographique OWNER TO geola;
ALTER TABLE referentiel.tr_sous_secteur_hydrographique OWNER TO geola;
ALTER TABLE referentiel.tr_taxon OWNER TO geola;
ALTER TABLE referentiel.tr_type_limite OWNER TO geola;
ALTER TABLE referentiel.tr_type_observation OWNER TO geola;
ALTER TABLE referentiel.tr_zone_hydrographique OWNER TO geola;

INSERT INTO geola.t_observation_ob 
(ob_id, 
ob_id_sig, 
ob_annee_debut,
ob_annee_fin, 
ob_presence, 
ob_coordonnees,
ob_nombre, 
ob_poids,
ob_comment_obs,
ob_comment_lieu,
ob_comment_abond,
ob_bv_id,
ob_rh_code,
ob_sh_code,
ob_ss_code,
ob_zh_code, 
ob_de_code,
ob_cm_id, 
ob_no_id,
ob_to_id, 
ob_ro_id, 
ob_ce_id, 
ob_ic_id,
ob_co_id,
ob_roe_code, 
ob_codehydro,
ob_ta_id, 
ob_commentinsert,
geom)
SELECT id_bdd AS ob_id, 
id_sig AS ob_id_sig, 
ob_annee_debut, 
ob_annee_fin, 
ob_presence::integer::boolean, 
ob_coordonnees, 
ob_nombre::numeric, 
ob_poids::numeric, 
ob_commentaire_obs AS ob_comment_obs,
ob_commentaire_lieu AS ob_comment_lieu,
ob_commentaire_abond AS ob_comment_abond,
bv_id AS ob_bv_id,
ob_rh_id AS ob_rh_code, 
ob_sh_id AS ob_sh_code, 
ob_ss_id AS ob_ss_code, 
ob_zh_id AS ob_zh_code,
ob_de_id AS ob_de_code,
cm_id, -- ob_cm_id contient le nom de la commune... ON vire
ob_no_id, --nature obs
ob_to_id::integer, --type obs
tr_reference_observation.ro_id, -- reference
ob_ce_id, -- cond env
ob_ic_id, 
co_id AS ob_co_id,
ob_roe_id AS ob_roe_code, 
code_hydro AS ob_codehydro , 
ob_ta_id, 
observation_coralie AS ob_commentinsert ,
NULL AS geom
FROM temp.observation
LEFT JOIN referentiel.tr_reference_observation
  ON tr_reference_observation.ro_source = observation.ob_ro_id
LEFT JOIN  referentiel.tr_commune 
  ON referentiel.tr_commune.cm_code = observation.insee; --2660

-- La table a été copiée depuis le shape dans temp 
  
ALTER TABLE "temp".obs_shp RENAME TO observation_pmc;
CREATE SCHEMA tempa
ALTER TABLE "temp".observation_pmc SET SCHEMA tempa;
ALTER TABLE tempa.observation_pmc OWNER TO geola;
-- Mise à jour des geom dans la table observation_pmc

-- verif de la jointure
select *
from tempa.observation_pmc
full outer JOIN geola.t_observation_ob ON observation.ob_id_sig=observation_pmc.ob_id;

 
-- mise à jour geom (toutes les données de observation_pmc (la couche shape) on un id_sig dans observation
update geola.t_observation_ob
set geom=observation_pmc.geom
from tempa.observation_pmc
where ob_id_sig=observation_pmc.ob_id;--1548

ALTER TABLE geola.t_observation_ob DROP COLUMN ob_coordonnees;
ALTER TABLE geola.t_observation_ob ADD COLUMN ob_XL93_2154 NUMERIC;
ALTER TABLE geola.t_observation_ob ADD COLUMN ob_YL93_2154 NUMERIC;

ALTER TABLE geola.t_observation_ob RENAME COLUMN ob_roe_code TO ob_id_roe;



ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_bv_id_fkey FOREIGN KEY (ob_bv_id) REFERENCES referentiel.tr_bassin_versant(bv_id);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_codehydro_id_fkey FOREIGN KEY (ob_codehydro) REFERENCES referentiel.tr_cours_d_eau(cd_code);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_ce_id_fkey FOREIGN KEY (ob_ce_id) REFERENCES referentiel.tr_condition_environnementale(ce_id);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_cm_id_fkey FOREIGN KEY (ob_cm_id) REFERENCES referentiel.tr_commune(cm_id);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_de_code_fkey FOREIGN KEY (ob_de_code) REFERENCES referentiel.tr_departement(de_code);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_ic_id_fkey FOREIGN KEY (ob_ic_id) REFERENCES referentiel.tr_indice_confiance(ic_id);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_no_id_fkey FOREIGN KEY (ob_no_id) REFERENCES referentiel.tr_nature_observation(no_id);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_rh_id_fkey FOREIGN KEY (ob_rh_code) REFERENCES referentiel.tr_region_hydrographique(rh_code);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_id_roe_fkey FOREIGN KEY (ob_id_roe) REFERENCES referentiel.tr_roe(roe_id_roe);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_sh_code_fkey FOREIGN KEY (ob_sh_code) REFERENCES referentiel.tr_secteur_hydrographique(sh_code);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_ss_code_fkey FOREIGN KEY (ob_ss_code) REFERENCES referentiel.tr_sous_secteur_hydrographique(ss_code);
ALTER TABLE geola.t_observation_ob ADD  CONSTRAINT observation_ob_cd_code_fkey FOREIGN KEY (ob_cd_code) REFERENCES referentiel.tr_cours_d_eau(cd_code);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_ta_id_fkey FOREIGN KEY (ob_ta_id) REFERENCES referentiel.tr_taxon(ta_id);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_to_id_fkey FOREIGN KEY (ob_to_id) REFERENCES referentiel.tr_type_observation(to_id);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_zh_code_fkey FOREIGN KEY (ob_zh_code) REFERENCES referentiel.tr_zone_hydrographique(zh_code);
ALTER TABLE geola.t_observation_ob ADD   CONSTRAINT observation_ob_ro_id FOREIGN KEY (ob_ro_id) REFERENCES referentiel.tr_reference_observation(ro_id)


