-- Importation de la couche geologie france dans la base eda2.3 pour les modèles MONTEPOMI saumon
-- Téléchargement du shape sur infoterre http://infoterre.brgm.fr/formulaire/telechargement-carte-geologique-metropolitaine-11-000-000
-- Ouverture de la couche GEO001M_CART_FR_S_FGEOL_2154.shp sous QGIS pour import de la couche sur serveur Cédric/eda2.3/schema france

--on créée un index 
CREATE INDEX indx_geol_geom ON france.geol USING gist (geom);
--On regarde les différentes valeurs du champ NATURE
select distinct nature
from france.geol;

/*
|nature                |
|----------------------|
|plutonique, volcanique|
|métamorphique         |
|volcano-plutonique    |
|plutonique            |
|non documenté         |
|volcanique            |
|composite             |
|sédimentaire          |

*/

--On regroupe ces différents type en 3 classes
	--sédimentaire
	--métamorphique+composite = socle
	--plutonique / volcanique / = pluto-volcanique

alter table france.geol add column geol_type text;
update france.geol set geol_type='sédimentaire' where nature='sédimentaire';
update france.geol set geol_type='socle' where nature in ('métamorphique','composite');
update france.geol set geol_type='volcanique' where nature in ('plutonique, volcanique','volcano-plutonique','plutonique','volcanique');

--On fait la jointure avec montepomi.rn_rna et on stocke dans une vue
drop view if exists france.rn_rna_geol;
create view france.rn_rna_geol AS(
With 
clipped as (
		 select r.idsegment,
		 		g.geol_type,
        		(ST_Dump(ST_Intersection(r.geom, st_transform(g.geom,3035)))).geom
         from france.rn_rna r
         inner join france.geol g on 
         ST_Intersects(r.geom, st_transform(g.geom,3035))
),
--SELECT count(*) FROM clipped  -- 196033
clippedsegment as(
    select clipped.idsegment, 
    		clipped.geol_type, 
    		clipped.geom
    from clipped
    where ST_Dimension(clipped.geom) = 1
), -- remvoe points
--SELECT count(*) FROM clippedsegment --196033
lengthclipped AS (
		select geol_type, 
				sum(st_length(c.geom)) as totalclippedlength, 
				idsegment 
		from clippedsegment c
        group by idsegment,geol_type
        order by idsegment, totalclippedlength desc
) 
--SELECT count(*) from lengthclipped --124452
select distinct on (idsegment) geol_type, totalclippedlength,idsegment
from lengthclipped 
) 

select count(*) from france.rn_rna_geol --114 556


