
# cedric 31/03 restauration en local des tables du schema geola


pg_dump -c --table geola.t_observation_ob --table geola.t_limite_lm --dbname=postgresql://USER:PWD@HOST:5432/geola  | psql --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola

# sauveragde du referentiel
pg_dump -c --schema referentiel --dbname=postgresql://USER:PWD@HOST:5432/geola  | psql --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola

#Léa sauvegarde des limites après travail pour insérer les données manquantes 31/03/2023
pg_dump -c --table geola.t_limite_lm --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola | psql  --dbname=postgresql://$env:usergeola:$env:passgeola@$env:hostgeola/geola 


# Cédric restauration de la table geola.t_limite_lm depuis le serveur 02/04/2023
# restauration avec Powersheel
# tapper path pour trouver les variables d'environnement et ajouter usergeola, passgeola, et hostgeola aux variables d'environnement de l'utilisateur
 pg_dump -c --table geola.t_limite_lm  --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola | psql  --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola
 
 # restauration de la table t_j_limit_observation sur le serveur
pg_dump -c --table geola.tj_limite_observation_lo  --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola | psql  --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola 
 
 
 # Cédric restauration de la table geola. depuis le serveur 02/04/2023
pg_dump -c --table geola.tj_limite_observation_lo --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola  | psql  --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola
 
 
 # Cédric sauvegarde pour Benjamin vers le drive
pg_dump -U $env:usergeola -h $env:hostgeola -c --schema geola -f schema_geola.backup -Fc geola
pg_restore -U postgres -d geola  schema_geola.backup
 
 
 # Jules sauve pour le serveur
 
pg_dump -c --table referentiel.tr_reference_observation --table geola.tj_limite_observation_lo --table geola.t_observation_ob --table tempa.t_limitsat --table geola.ce_ala --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola | psql --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola

 # pour Benjamin
pg_dump --schema geola --schema referentiel -Fc --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola -f "backup_complet_05042023.backup"
pg_restore -d geola --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola -f "backup_complet_05042023.backup"
  

psql -c "ALTER TABLE geola.t_observation_ob DROP CONSTRAINT observation_ob_ro_id ;" --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola 


pg_dump -f "geola.t_observation_ob_05042023.backup" --table geola.t_observation_ob -Fc --verbose --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola 
pg_dump -f "geola.t_limite_lm_ob_05042023.backup" --table geola.t_limite_lm -Fc --verbose --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola 
psql -c " DROP TABLE geola.t_observation_ob CASCADE ;" --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola 
psql -c "DROP TABLE geola.t_limite_lm CASCADE ;" --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola 

# lea

pg_dump -c --table referentiel.tr_indice_confiance --table referentiel.tr_taxon --table referentiel.tr_reference_observation --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola |  psql --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola 
pg_dump --schema geola --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola |  psql --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola 

# Jules sauvegarde table points_nouveaux_ala sur serveur
pg_dump -c --table tempa.points_nouveaux_ala --table tempa.points_nouveaux --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola | psql --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola

# Cédric

pg_dump -c --table tempa.points_nouveaux_ala --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola | psql  --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola
pg_dump --schema geol --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola | psql  --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola

# CEDRIC pour Benjamin 06/04 (travail clé primaires, valeurs manquantes, cours d'eau cogepomi ...;

pg_dump --schema geol --schema referentiel -Fc --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola -f "backup_complet_06042023.backup"
psql -c "DROP SCHEMA geol cascade" --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola
psql -c "CREATE SCHEMA geol" --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola

pg_dump --schema geol --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola | psql --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola


pg_dump --schema geol -Fc --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola -f "backup_geol_06042023.backup"

# lea restore local
pg_dump --schema geol --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola | psql --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola

#lea save db 01/06/2023 :
cd C:\Users\lea.patau\OneDrive - EPTB Vilaine\Documents\base de donnée
pg_dump  --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola -- schema geol > geola.sql

# cedric restauration de la table t_limit_lm renommee geol.t_limit_lmsauv depuis localhost to server
pg_dump -c --table geol.t_limit_lmsauv --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geola | psql --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola

pg_dump -c --table geola.t_limite_lmsauv --dbname=postgresql://postgres:postgres@127.0.0.1:5432/geolasauv | psql --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola

pg_dump -U postgres --table geol.t_limite_lm --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola -Fc -f "dump_geola_t_limit_lm_0506.sql" --verbose 

D:
cd D:\eda\montepomi
pg_dump  --dbname=postgresql://${env:usergeola}:${env:passgeola}@$env:hostgeola/geola --schema geol > geola_2024.sql

shp2pgsql -s 2154 -W "latin1" -I -k "bgmlinsatcolonise2020.shp" montepomi.bgmlinsatcolonise > bgmlinsatcolonise.sql
psql -f bgmlinsatcolonise.sql --dbname=postgresql://${env:usermercure}:${env:passmercure}@${env:hostmercure}:5432/eda2.3 
